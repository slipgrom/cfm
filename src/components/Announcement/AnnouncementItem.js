import React, { Component, PropTypes } from 'react'
import { connect } from 'react-redux'

import ItemImage from 'components/Collection/ItemImage'
import EmailForm from './EmailForm'
import Countdown from './Countdown'

const mapStateToProps = (state) => {
  const locale = state.locales.langs[state.locales.selectedLocale]
  const i18n = state.i18n[locale.title]

  return {
    announcement: state.announcement,
    i18n: i18n.announcement
  }
}

export class AnnouncementItem extends Component {
  static propTypes = {
    type: PropTypes.string.isRequired,
    announcement: PropTypes.object.isRequired,
    i18n: PropTypes.object.isRequired
  };

  render () {
    const { type, i18n, announcement } = this.props
    const announcementStyle = { backgroundImage: `url(${announcement.bgSrc})` }
    const { title } = i18n[type]

    return (
      <div className='listing-item listing-item_announcement' style={announcementStyle}>
        <div className='listing-item_announcement__img'><ItemImage type={type} /></div>
        <div className='listing-item_announcement__content'>
          <h3>{title}</h3>
          <Countdown releaseDate={announcement.releaseDate} />
          <div className='listing-item_announcement__input'><EmailForm onSend={null} title={i18n.shortInputTitle} placeholder={i18n.placeholder} /></div>
        </div>
      </div>
    )
  }
}

export default connect(mapStateToProps)(AnnouncementItem)
