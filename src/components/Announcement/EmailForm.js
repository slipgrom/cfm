import React, { PropTypes } from 'react'

import { RightArrow } from 'components/Common/IconsCollection'

export class EmailForm extends React.Component {
  static propTypes = {
    onSend: PropTypes.func,
    title: PropTypes.string,
    placeholder: PropTypes.string
  };

  constructor (...args) {
    super(args)

    this.handleFocus = this.handleFocus.bind(this)
    this.handleBlur = this.handleBlur.bind(this)
  }

  render () {
    const { title, placeholder } = this.props
    return (
      <form className='subscribe-form'>
        <div className='input' onMouseOver={this.handleFocus} onMouseOut={this.handleBlur}>
          <input ref='subscribeInput' id='subscribe-input' onFocus={this.handleFocus} onBlur={this.handleBlur} type='text' placeholder={placeholder} />
          <label ref='subscribeLabel' htmlFor='subscribe-input'>{title}</label>
        </div>
        <button className='button'><RightArrow strokeColor='#fff' /></button>
      </form>
    )
  }

  handleFocus (e) {
    this.refs.subscribeLabel.style.display = 'none'
  }

  handleBlur (e) {
    if (this.refs.subscribeInput !== document.activeElement) {
      this.refs.subscribeLabel.style.display = 'block'
    }
  }
}

export default EmailForm
