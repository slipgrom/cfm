import React, { Component, PropTypes } from 'react'

import moment from 'moment'
import countdown from 'countdown'
import { plural } from 'helpers/helpers'

export class Countdown extends Component {
  static propTypes = {
    releaseDate: PropTypes.string.isRequired
  };

  constructor (...args) {
    super(args)

    this.state = {
      timespan: null
    }
  }

  componentWillMount () {
    const { releaseDate } = this.props
    this.t = setInterval(() => {
      let ts = countdown(moment(), moment(releaseDate))
      this.setState({
        timespan: ts
      })
    }, 1000)
  }

  componentWillUnmount () {
    clearInterval(this.t)
  }

  render () {
    const { timespan } = this.state
    return timespan && <div className='countdown'>
      {this.renderDays()} : {this.renderHours()} : {this.renderMinutes()} : {this.renderSeconds()}
    </div>
  }

  renderDays () {
    return (
      <div className='countdown-block'>
        {this.formatNumber(this.state.timespan.days)}
        <div className='countdown-text'>{plural(this.state.timespan.days, 'день', 'дня', 'дней')}</div>
      </div>
    )
  }

  renderHours () {
    return (
      <div className='countdown-block'>
        {this.formatNumber(this.state.timespan.hours)}
        <div className='countdown-text'>{plural(this.state.timespan.hours, 'час', 'часа', 'часов')}</div>
      </div>
    )
  }

  renderMinutes () {
    return (
      <div className='countdown-block'>
        {this.formatNumber(this.state.timespan.minutes)}
        <div className='countdown-text'>{plural(this.state.timespan.minutes, 'минуту', 'минуты', 'минут')}</div>
      </div>
    )
  }

  renderSeconds () {
    return (
      <div className='countdown-block'>
        {this.formatNumber(this.state.timespan.seconds)}
        <div className='countdown-text'>{plural(this.state.timespan.seconds, 'секунду', 'секунды', 'секунд')}</div>
      </div>
    )
  }

  formatNumber (number) {
    return (number < 10 ? '0' : '') + number
  }
}

export default Countdown
