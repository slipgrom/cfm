import React, { PropTypes } from 'react'
import { connect } from 'react-redux'

import CatMask from 'components/Common/CatMask'
import CollectionBadge from 'components/Common/CollectionBadge'
import EmailForm from './EmailForm'
import Countdown from './Countdown'

const mapStateToProps = (state) => {
  const locale = state.locales.langs[state.locales.selectedLocale]
  const i18n = state.i18n[locale.title]

  return {
    announcement: state.announcement,
    i18n: i18n.announcement
  }
}

export class Announcement extends React.Component {
  static propTypes = {
    i18n: PropTypes.object.isRequired,
    announcement: PropTypes.object.isRequired
  };

  render () {
    const { i18n, announcement } = this.props
    const announcementStyle = { backgroundImage: `url(${announcement.bgSrc})` }

    return (
      <div className='collection collection--announcement' style={announcementStyle}>
        <CatMask src='/images/announcement/cat.png' />
        <CollectionBadge type='announcement' />
        <h3 className='collection--title'>{announcement.title}</h3>
        <h5>До выхода коллекции осталось:</h5>
        <Countdown releaseDate={announcement.releaseDate} />

        <EmailForm onSend={null} title={i18n.largeInputTitle} placeholder={i18n.placeholder} />
      </div>
    )
  }
}

export default connect(mapStateToProps)(Announcement)
