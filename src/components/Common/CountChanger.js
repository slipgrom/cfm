import React, { PropTypes } from 'react'

export class CountChanger extends React.Component {
  static propTypes = {
    count: PropTypes.number.isRequired,
    onChangeCount: PropTypes.func.isRequired
  };

  render () {
    const { count, onChangeCount } = this.props

    return (
      <div className='count-changer'>
        <span className='action' onClick={(e) => {
          e.preventDefault()
          let c = Math.max(count - 1, 0)
          onChangeCount(c)
        }}>-</span>
        <span className='text'>Количество {count}</span>
        <span className='action' onClick={(e) => {
          e.preventDefault()
          let c = count + 1
          onChangeCount(c)
        }}>+</span>
      </div>
    )
  }
}

export default CountChanger
