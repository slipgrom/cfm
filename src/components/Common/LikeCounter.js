import React, { PropTypes } from 'react'

export class Collection extends React.Component {
  static propTypes = {
    count: PropTypes.number.isRequired
  };

  constructor (...args) {
    super(...args)
  }

  render () {
    return (
      <div className='likes-counter'>
        <span>{this.props.count}</span><img src='/images/cat_likes.svg' />
      </div>
    )
  }
}

export default Collection
