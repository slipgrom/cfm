import React from 'react'

import { BasketIcon } from 'components/Common/IconsCollection'

export class MoreButton extends React.Component {
  render () {
    return (
      <span className='more-button'>Подробнее<BasketIcon /></span>
    )
  }
}

export default MoreButton
