import React, { PropTypes } from 'react'

export default class IosSwitcher extends React.Component {
  static propTypes = {
    active: PropTypes.bool.isRequired,
    onClick: PropTypes.func.isRequired
  };

  render () {
    const classNames = ['ios-switcher']

    if (this.props.active) {
      classNames.push('active')
    }

    return (
      <div className={classNames.join(' ')} onClick={this.props.onClick}></div>
    )
  }
}
