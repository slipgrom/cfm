import React, { PropTypes, Component } from 'react'

export class ImageTshirt extends Component {
  static propTypes = {
    collectionId: PropTypes.number
  };

  render () {
    const { collectionId } = this.props
    let src = '/images/tshirt.png'

    if (collectionId) {
      src = `/images/collection${collectionId}/tshirt.png`
    }
    return (
      <img className='tshirt' src={src} />
    )
  }
}

export default ImageTshirt
