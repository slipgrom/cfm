import React, { PropTypes } from 'react'

export class CollectionBadge extends React.Component {
  static propTypes = {
    type: PropTypes.string.isRequired
  };

  constructor (...args) {
    super(...args)
  }

  render () {
    const className = `collection-badge collection-badge--${this.props.type}`

    return (
      <div className={className}>
        <div className='collection-badge--title'>{this.renderTitle()}</div>
        <div className='collection-badge--points-wrap'>
          {this.renderBadge()}
        </div>
      </div>
    )
  }

  renderTitle () {
    const titles = {
      'new': 'New!',
      'hot': 'Hot!',
      'announcement': 'Анонс',
      'favorite': ''
    }

    return titles[this.props.type]
  }

  renderBadge () {
    const pointsClassName = `collection-badge--points collection-badge--points-${this.props.type}`

    const path = `M42.736,748l-2.072,3.622-3.343-2.466,0,0-1.285,3.973-3.755-1.719v0.006l-0.456,4.158L27.77,
    754.7l0-.007,0.441,4.161-4.152-.013v0l1.295,3.973-4.072.855-0.007,0,2.077,3.643-3.766,1.629-0.005.011,
    2.784,3.111L19,774.5l0,0,3.37,2.453-2.788,3.086v0l3.808,1.7-2.1,3.611v0l4.064,0.86-1.3,3.969,0-.007,
    4.157,0-0.447,4.152v-0.01l4.06-.869,0.388,4.139,0,0,3.8-1.706,1.27,3.977,0,0,3.367-2.459,2.048,
    3.616h0l2.785-3.105,2.744,3.105h0l2.088-3.622,3.335,2.465,0,0,1.284-3.974,3.763,1.711,0.008,0,0.447-4.158,
    4.042,0.906,0,0-0.441-4.152,4.152,0.013v0l-1.279-4,4.047-.83,0.007,0L67.6,781.747l3.8-1.686,0.013,
    0.006-2.792-3.112,3.367-2.411,0,0-3.37-2.478,2.8-3.062v0l-3.808-1.718,2.087-3.611,0.007,0-4.072-.859,
    1.312-3.944,0,0-4.174-.011,0.447-4.144h0l-4.06.87-0.413-4.147,0,0-3.8,1.706-1.262-3.969H53.7l-3.367,
    2.459-2.056-3.616h-0.01l-2.768,3.1L42.736,748h0`

    return (
      <svg width='53' height='53' viewBox='0 0 53 53'>
        <path d={path}
              fill='#fff'
              transform='translate(-19 -748)' />
        <circle className={pointsClassName}
                cx='21'
                cy='21'
                r='21'
                transform='translate(5 5)'
                 />
      </svg>
    )
  }
}

export default CollectionBadge
