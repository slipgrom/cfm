import React, { PropTypes, Component } from 'react'

export class ImageWallpaper extends Component {
  static propTypes = {
    collectionId: PropTypes.number,
    style: PropTypes.string
  };

  render () {
    const { collectionId, style } = this.props
    let src = `/images/empty.gif`

    if (collectionId && style) {
      src = `/images/collection${collectionId}/${style}/wallpaper.jpg`
    }

    return (
      <img className='wallpaper' src={src} />
    )
  }
}

export default ImageWallpaper
