import React, { PropTypes } from 'react'
import _ from 'lodash'

export class StyleSwitcher extends React.Component {
  static propTypes = {
    style: PropTypes.string,
    children: PropTypes.array.isRequired,
    activeElementIndex: PropTypes.number.isRequired,
    elWidth: PropTypes.number,
    className: PropTypes.string
  };

  constructor (...args) {
    super(...args)

    this.setActiveIndex = this.setActiveIndex.bind(this)
  }

  componentDidMount () {
    const index = this.props.activeElementIndex
    const items = this.refs.switcher.getElementsByTagName('a')
    const item = items[index]

    if (this.props.elWidth) {
      _.forEach(items, (item) => {
        item.style.width = `${this.props.elWidth}px`
      })
    }

    setTimeout(() => {
      this.setActiveIndex(item)
    }, 10)
  }

  componentWillReceiveProps (nextProps) {
    const index = nextProps.activeElementIndex
    const items = this.refs.switcher.getElementsByTagName('a')
    const item = items[index]

    _.forEach(items, (item) => {
      item.classList.remove('switcher--active-el')
    })

    this.setActiveIndex(item)
  }

  render () {
    let className = ['switcher']
    className.push(this.props.className)

    if (this.props.style) {
      className.push(`switcher-${this.props.style}`)
    }

    return (
      <div className={className.join(' ')}>
        <div ref='switcher' className='switcher-wrap'>
          {this.props.children}
          <span className='switcher--active-bg'></span>
        </div>
      </div>
    )
  }

  setActiveIndex (item) {
    item.className = 'switcher--active-el'

    const activeBgEl = this.refs.switcher.getElementsByClassName('switcher--active-bg')[0]
    activeBgEl.style.left = `${item.offsetLeft}px`
    activeBgEl.style.width = `${this.props.elWidth || item.offsetWidth}px`
  }
}

export default StyleSwitcher
