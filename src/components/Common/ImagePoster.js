import React, { PropTypes, Component } from 'react'

export class ImagePoster extends Component {
  static propTypes = {
    collectionId: PropTypes.number,
    style: PropTypes.string
  };

  render () {
    const { collectionId, style } = this.props
    let src = `/images/empty.gif`

    if (collectionId && style) {
      src = `/images/collection${collectionId}/${style}/poster.jpg`
    }

    return (
      <img className='poster' src={src} />
    )
  }
}

export default ImagePoster
