import React from 'react'
import { Link } from 'react-router'

export class ShowMoreCollections extends React.Component {
  render () {
    return (
      <Link to='/ru/vintage/collections' className='large-button large-button-filled'>Показать все коллекции</Link>
    )
  }
}

export default ShowMoreCollections
