import React, { PropTypes } from 'react'

export class Collection extends React.Component {
  static propTypes = {
    src: PropTypes.string.isRequired
  };

  render () {
    return (
      <img className='cat-mask' src={this.props.src} />
    )
  }
}

export default Collection
