import React, { PropTypes } from 'react'
import { Link } from 'react-router'

import {
  TSHIRT,
  WALLPAPER,
  POSTER
} from 'redux/modules/collections'

export const ItemBanners = ({ currentType, lang, style }) => (
  <div className='item-banners'>
    {(currentType !== TSHIRT) && <Link to={`/${lang}/${style}/tshirts`} className='item-banner item-banner--tshirts'>Футболки</Link>}
    {(currentType !== POSTER) && <Link to={`/${lang}/${style}/posters`} className='item-banner item-banner--posters'>Постеры</Link>}
    {(currentType !== WALLPAPER) && <Link to={`/${lang}/${style}/wallpapers`} className='item-banner item-banner--wallpapers'>Wallpapers</Link>}
  </div>
)

ItemBanners.propTypes = {
  currentType: PropTypes.string.isRequired,
  lang: PropTypes.string.isRequired,
  style: PropTypes.string.isRequired
}

export default ItemBanners
