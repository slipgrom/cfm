import React, { PropTypes, Component } from 'react'

export class CollectionItemImage extends Component {
  static propTypes = {
    classes: PropTypes.string.isRequired,
    url: PropTypes.string.isRequired
  };

  render () {
    const { url, classes } = this.props

    return (
      <div className={classes}>
        <img src={url} />
      </div>
    )
  }
}

export default CollectionItemImage
