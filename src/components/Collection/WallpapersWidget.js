import React, { Component } from 'react'
import MediaQuery from 'react-responsive'

export class WallpapersWidget extends Component {
  constructor (...args) {
    super(...args)

    this.state = {
      activeIndex: 0
    }
  }

  render () {
    const { activeIndex } = this.state
    const iphoneGalleryStyle = {
      transform: `translateX(-${206 * activeIndex}px)`
    }
    const androidGalleryStyle = {
      transform: `translateX(-${189 * activeIndex}px)`
    }

    return (
      <div className='wallpapers-widget'>
        <div className='wallpapers-widget--phones'>
          <div className='iphone'>
            <div className='gallery'>
              <div style={iphoneGalleryStyle} className='slides'>
                <img src='/images/wallpapersWidget/iphone-slide-1.jpg' alt='' />
                <img src='/images/wallpapersWidget/iphone-slide-1.jpg' alt='' />
                <img src='/images/wallpapersWidget/iphone-slide-1.jpg' alt='' />
              </div>
            </div>
          </div>
          <div className='android'>
            <div className='gallery'>
              <div style={androidGalleryStyle} className='slides'>
                <img src='/images/wallpapersWidget/android-slide-1.jpg' alt='' />
                <img src='/images/wallpapersWidget/android-slide-1.jpg' alt='' />
                <img src='/images/wallpapersWidget/android-slide-1.jpg' alt='' />
              </div>
            </div>
          </div>
        </div>

        <div className='wallpapers-widget--text'>
          <h3>Скачать бесплатно</h3>
          <a className='appStore' href='#'>App store</a>
          <a className='googlePlay' href='#'>Google play</a>

          <MediaQuery query='(max-width: 960px)'>
            <div className='wallpapers-widget--links'>
              <div className={activeIndex === 0 ? 'active' : null}>Удобный магазин коллекций</div>
              <div className={activeIndex === 1 ? 'active' : null}>Оповещение выхода коллекций</div>
              <div className={activeIndex === 2 ? 'active' : null}>Сервис обоев</div>
              <span className='dots'>
                <a href='#' className={activeIndex === 0 ? 'active' : null} onClick={this.handleClick.bind(this, 0)}></a>
                <a href='#' className={activeIndex === 1 ? 'active' : null} onClick={this.handleClick.bind(this, 1)}></a>
                <a href='#' className={activeIndex === 2 ? 'active' : null} onClick={this.handleClick.bind(this, 2)}></a>
              </span>
            </div>
          </MediaQuery>
          <MediaQuery query='(min-width: 961px)'>
            <div className='wallpapers-widget--links'>
              <div><a href='#' className={activeIndex === 0 ? 'active' : null} onClick={this.handleClick.bind(this, 0)}>Удобный магазин коллекций</a></div>
              <div><a href='#' className={activeIndex === 1 ? 'active' : null} onClick={this.handleClick.bind(this, 1)}>Оповещение выхода коллекций</a></div>
              <div><a href='#' className={activeIndex === 2 ? 'active' : null} onClick={this.handleClick.bind(this, 2)}>Сервис обоев</a></div>
            </div>
          </MediaQuery>
        </div>
      </div>
    )
  }

  handleClick (index, e) {
    e.preventDefault()
    this.setState({
      activeIndex: index
    })
  }
}

export default WallpapersWidget
