import React, { PropTypes, Component } from 'react'

import ImageTshirt from 'components/Common/ImageTshirt'
import ImageWallpaper from 'components/Common/ImageWallpaper'
import ImagePoster from 'components/Common/ImagePoster'

import {
  TSHIRT,
  WALLPAPER,
  POSTER
} from 'redux/modules/collections'

export class ItemImage extends Component {
  static propTypes = {
    collectionId: PropTypes.number,
    type: PropTypes.string.isRequired,
    style: PropTypes.string
  };

  render () {
    const { collectionId, type, style } = this.props

    const images = {
      [TSHIRT]: (<ImageTshirt collectionId={collectionId} style={style} />),
      [WALLPAPER]: (<ImageWallpaper collectionId={collectionId} style={style} />),
      [POSTER]: <ImagePoster collectionId={collectionId} style={style} />
    }

    return images[type]
  }
}

export default ItemImage
