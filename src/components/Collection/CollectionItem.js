import React, { PropTypes } from 'react'

import MoreButton from 'components/Common/MoreButton'
import { RubleSign } from 'components/Common/IconsCollection'
import ItemImage from './ItemImage'

export class CollectionItem extends React.Component {
  static propTypes = {
    item: PropTypes.object.isRequired,
    collectionId: PropTypes.number,
    style: PropTypes.string
  };

  render () {
    const { item, collectionId, style } = this.props
    const classNames = `collection-item collection-item--${item.type}`
    let priceText = 'Free!'

    if (item.price) {
      priceText = (<span>{item.price} <RubleSign /></span>)
    }

    const price = (<div className='collection-item--price'>{priceText}</div>)

    return (
      <div className={classNames}>
        {price}
        <span>
          <MoreButton />
        </span>
        <ItemImage collectionId={collectionId} type={item.type} style={style} />
      </div>
    )
  }
}

export default CollectionItem
