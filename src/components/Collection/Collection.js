import React, { PropTypes } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router'

import CatMask from '../Common/CatMask'
import LikeCounter from '../Common/LikeCounter'
import CollectionBadge from '../Common/CollectionBadge'
import CollectionItem from './CollectionItem'

export class Collection extends React.Component {
  static propTypes = {
    style: PropTypes.string.isRequired,
    lang: PropTypes.string.isRequired,
    collection: PropTypes.object.isRequired
  };

  constructor (...args) {
    super(...args)

    this.renderItems = this.renderItems.bind(this)
  }

  render () {
    const { collection, style, lang } = this.props

    const collectionStyle = {
      backgroundImage: `url(/images/collection${collection.id}/bg.jpg)`
    }

    return (
      <div className='collection'
           style={collectionStyle}>
        <CatMask src={collection.catSrc[style]} />
        <CollectionBadge type='hot' />
        <h3 className='collection--title'>{collection.title}</h3>
        <LikeCounter count={collection.likes} />

        <div className='collection-items-wrap'>
          <ul className='collection-items'>
            {collection.items.map((item, index) => {
              return this.renderItems(item, collection.id, style, index, lang)
            })}
          </ul>
        </div>
      </div>
    )
  }

  renderItems (item, collectionId, style, index, lang) {
    const itemLink = `/${lang}/${style}/collections/${collectionId}/${item.type}`

    return (
      <li key={index}>
        <Link to={itemLink}>
          <CollectionItem item={item} collectionId={collectionId} style={style} />
        </Link>
      </li>
    )
  }
}

export default connect()(Collection)
