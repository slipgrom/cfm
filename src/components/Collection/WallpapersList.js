import React, { PropTypes, Component } from 'react'
import { connect } from 'react-redux'

import _ from 'lodash'

const mapStateToProps = (state) => {
  // const list = _.map(state.collections, (collection) => {
  //   return _.find(collection.items, { type: WALLPAPER })
  // })
  return {
    wallpapersList: state.widgets.wallpapersList
  }
}

export class WallpapersList extends Component {
  static propTypes = {
    collectionId: PropTypes.number.isRequired,
    style: PropTypes.string.isRequired,
    wallpapersList: PropTypes.array.isRequired
  };

  constructor (...args) {
    super(...args)

    this.state = {
      list: []
    }

    this.handleClick = this.handleClick.bind(this)
  }

  componentDidMount () {
    const { wallpapersList, collectionId } = this.props
    const currentList = this.sortList(wallpapersList, collectionId)

    this.setState({
      list: currentList
    })
  }

  render () {
    const { style } = this.props

    return (
      <div className='wallpapers-list'>
        <div className='wallpapers-list--iphone'></div>
        <div className='wallpapers-list--wallpapers-wrap'>
          <div className='wallpapers-list--wallpapers'>
            {this.state.list.map((wallpaper, k) => {
              const src = wallpaper[style]
              return (<img key={k} src={src} className={k === 2 ? 'active' : ''} onClick={this.handleClick.bind(this, wallpaper.collectionId, k)} />)
            })}
          </div>
        </div>
      </div>
    )
  }

  sortList (list, collectionId) {
    const current = _.remove(list, { collectionId: collectionId })
    list.splice(2, 0, current[0])

    return list
  }

  handleClick (collectionId, index) {
    const { list } = this.state
    if (index === 2) {
      return false
    }

    if (index < 2) {
      const diffs = list.splice(list.length - (2 - index))
      this.setState({
        list: _.concat(diffs, list)
      })
      return false
    } else {
      const diffs = list.splice(0, index - 2)
      this.setState({
        list: _.concat(list, diffs)
      })
      return false
    }
  }
}

export default connect(mapStateToProps)(WallpapersList)
