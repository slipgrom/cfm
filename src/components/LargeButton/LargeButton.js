import React, { PropTypes } from 'react'

export class LargeButton extends React.Component {
  static propTypes = {
    text: PropTypes.string.isRequired
  };

  render () {
    return (
      <a href='#' className='large-button'>{this.props.text}</a>
    )
  }
}

export default LargeButton
