import React, { PropTypes } from 'react'
import { connect } from 'react-redux'

import { LeftGalleryArrow, RightGalleryArrow, InstIcon } from 'components/Common/IconsCollection'

export class InstagramBlock extends React.Component {
  static propTypes = {
    i18n: PropTypes.object.isRequired,
    tag: PropTypes.string.isRequired,
    photos: PropTypes.array.isRequired
  };

  constructor (...args) {
    super(...args)

    this.state = {
      photosWidth: 0
    }
  }

  componentWillMount () {
    const { photos } = this.props
    const width = 273
    const margin = 33

    this.setState({
      photosWidth: photos.length * (width + margin)
    })
  }

  render () {
    const { i18n, tag, photos } = this.props

    const photosStyle = {
      width: `${this.state.photosWidth}px`
    }

    return (
      <div className='instagram-block'>
        <div>
          <span className='inst-title'><InstIcon />{i18n.collection.instagram.title}</span>

          <span className='inst-arrows'>
            <span className='inst-arrow'>
              <LeftGalleryArrow />
            </span>
            <span className='inst-arrow'>
              <RightGalleryArrow />
            </span>
          </span>
        </div>

        <div className='photos' style={photosStyle}>
          {photos.map(this.renderPhoto)}
        </div>

        <div className='instagram-block--footer'>
          <span className='tag'>#{tag}</span>
          <span className='text' dangerouslySetInnerHTML={{__html: i18n.collection.instagram.footerText.replace('${tag}', tag)}}></span>
        </div>
      </div>
    )
  }

  renderPhoto (photoUrl, k) {
    return (
      <div key={k} className='photo'>
        <img src={photoUrl} />
      </div>
    )
  }
}

function mapStateToProps (state, ownProps) {
  const locale = state.locales.langs[state.locales.selectedLocale]
  const i18n = state.i18n[locale.title]

  return {
    i18n: i18n
  }
}

export default connect(mapStateToProps)(InstagramBlock)
