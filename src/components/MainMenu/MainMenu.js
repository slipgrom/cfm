import React, { PropTypes } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router'
import { random } from 'lodash'

import { setLocale } from 'redux/modules/locales'
import { setSiteStyle } from 'redux/modules/menus'
import { VINTAGE, MODERN } from 'redux/modules/collections'

import Badge from './Badge'
import StyleSwitcher from '../Common/StyleSwitcher'
import Logo from './Logo'

export class MainMenu extends React.Component {
  static propTypes = {
    mainMenu: PropTypes.object.isRequired,
    siteStyle: PropTypes.number.isRequired,
    locales: PropTypes.object.isRequired,
    setLocale: PropTypes.func.isRequired,
    setSiteStyle: PropTypes.func.isRequired,
    basketItems: PropTypes.number.isRequired,
    activeLang: PropTypes.string.isRequired,
    activeStyle: PropTypes.string.isRequired,
    onStyleChange: PropTypes.func,
    onLangChange: PropTypes.func,
    i18n: PropTypes.object,
    collections: PropTypes.array.isRequired
  };

  constructor (...args) {
    super(...args)

    this.renderLanguages = this.renderLanguages.bind(this)
    this.renderBasketLink = this.renderBasketLink.bind(this)
    this.renderMenuItem = this.renderMenuItem.bind(this)

    this.handleStyleClick = this.handleStyleClick.bind(this)
  }

  get catSrc () {
    const { collections, activeStyle } = this.props
    const catArray = collections.map((collection) => {
      return collection.catSrc[activeStyle]
    })

    return catArray[random(catArray.length - 1)]
  }

  render () {
    const { mainMenu: { items }, activeStyle, activeLang, i18n } = this.props

    const styleIndex = activeStyle === MODERN ? 0 : 1

    return (
      <div className='main-menu'>
        <ul className='main-menu--langs'>
          {this.renderLanguages()}
        </ul>
        <Link to={`/${activeLang}/${activeStyle}`} className='main-menu--logo'>
          <Logo src={this.catSrc} />
        </Link>
        {this.renderBasketLink()}
        <ul className='main-menu--items'>
          {items.map(this.renderMenuItem)}
        </ul>
        <div className='switcher-container'>
          <StyleSwitcher activeElementIndex={styleIndex}
                         elWidth={80}>
            <a href='#' onClick={() => this.props.onStyleChange(MODERN)}>{i18n.modern}</a>
            <a href='#' onClick={() => this.props.onStyleChange(VINTAGE)}>{i18n.vintage}</a>
          </StyleSwitcher>
        </div>
      </div>
    )
  }

  renderLanguages () {
    const { activeLang, locales: { langs } } = this.props

    return Object.keys(langs).map((k) => {
      const item = langs[k]
      let innerTag = (<a href='#'>{item.title}</a>)

      if (item.title === activeLang) {
        innerTag = (<span>{item.title}</span>)
      }

      return (<li key={k} className='main-menu--lang' onClick={this.changeLocale.bind(this, item.title)}>{innerTag}</li>)
    })
  }

  changeLocale (lang) {
    this.props.onLangChange(lang)
  }

  renderBasketLink () {
    const { basketItems, activeLang, activeStyle, i18n } = this.props

    return (
      <Link to={`/${activeLang}/${activeStyle}/basket`} className='main-menu--basket-link'>
        <svg className='basket-logo' width='60' height='60' viewBox='0 0 60 60'>
          <circle className='basket-logo--circle' cx='30' cy='30' r='30' fill='#fff' />
          <path fill='#000'
                d='M44.5,21h-9c-1.657,0-2.5,1.093-2.5,2.75V26a2.993,2.993,0,0,0,.405,1.5h1.141a2.239,2.239,0,0,1-.578-1.5V23.75c0-1.237.294-1.75,1.531-1.75h9c1.237,0,1.5.512,1.5,1.75V26a2.239,2.239,0,0,1-.578,1.5H46.6A2.993,2.993,0,0,0,47,26V23.75C47,22.093,46.158,21,44.5,21Zm-2.25,2.031h-4.5a0.75,0.75,0,0,1-.75-0.75V20.75A0.75,0.75,0,0,1,37.75,20h4.5a0.75,0.75,0,0,1,.75.75v1.531A0.75,0.75,0,0,1,42.25,23.031ZM45.547,38H34.453a2.237,2.237,0,0,1-2.2-1.762l-1.225-6.265A1.426,1.426,0,0,1,31,29.668V27.5A1.5,1.5,0,0,1,32.5,26h15A1.5,1.5,0,0,1,49,27.5v2.168a1.494,1.494,0,0,1-.037.329l-1.216,6.223A2.242,2.242,0,0,1,45.547,38ZM33,28v1.668L34.038,35.4a0.741,0.741,0,0,0,.728.568H45.234a0.746,0.746,0,0,0,.733-0.587l1.061-5.7,0-1.686H33Zm16,3H32V30H49v1Z'
                transform='translate(-26 -10) scale(1.4)' />
        </svg>
        <span className='main-menu--basket-link--text'>{i18n.basket}</span>
        {basketItems ? <Badge count={basketItems} /> : null}
      </Link>
    )
  }

  renderMenuItem (item, index) {
    const { activeLang, activeStyle, i18n } = this.props

    return (
      <li key={index} className={`item${index}`}>
        <Link to={`/${activeLang}/${activeStyle}/${item.url}`}>{i18n.mainMenu[item.url]}</Link>
      </li>
    )
  }

  handleStyleClick (style) {
    this.props.setSiteStyle(style)
  }
}

function mapStateToProps (state) {
  return {
    mainMenu: state.menus.mainMenu,
    siteStyle: state.menus.siteStyle,
    locales: state.locales,
    basketItems: state.basket.items.length,
    collections: state.collections
  }
}

export default connect(mapStateToProps, {
  setLocale,
  setSiteStyle
})(MainMenu)
