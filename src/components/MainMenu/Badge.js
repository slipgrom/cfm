import React, { PropTypes } from 'react'
import TweenMax from 'gsap'

export class Badge extends React.Component {
  static propTypes = {
    count: PropTypes.number.isRequired
  };

  componentDidMount (prevProps, prevState) {
    this.animate()
  }

  componentDidUpdate (prevProps, prevState) {
    this.animate()
  }

  render () {
    return (
      <div className='badge'
           ref='badge'>
        {this.props.count}
      </div>
    )
  }

  animate () {
    TweenMax.fromTo(this.refs.badge, 0.5, {scale: 0}, {scale: 1})
  }
}

export default Badge
