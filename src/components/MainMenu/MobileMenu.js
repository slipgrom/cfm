import React, { PropTypes } from 'react'
import { connect } from 'react-redux'

import { MobileMenuIcon, CloseIcon, ArrowDownIcon } from '../Common/IconsCollection'
import { setLocale } from 'redux/modules/locales'

export class MobileMenu extends React.Component {
  static propTypes = {
    locales: PropTypes.object.isRequired,
    mainMenu: PropTypes.object.isRequired,
    topMenu: PropTypes.object.isRequired,
    setLocale: PropTypes.func.isRequired
  };

  constructor (...args) {
    super(...args)

    this.renderMenu = this.renderMenu.bind(this)
    this.renderLanguages = this.renderLanguages.bind(this)
    this.renderMenuItem = this.renderMenuItem.bind(this)
    this.handleClickMenuIcon = this.handleClickMenuIcon.bind(this)
    this.handleClickCloseIcon = this.handleClickCloseIcon.bind(this)
    this.handleLangClick = this.handleLangClick.bind(this)
  }

  render () {
    return (
      <div>
        <div className='mobile-menu'>
          <div className='mobile-menu--icon' onClick={this.handleClickMenuIcon}>
            <MobileMenuIcon />
          </div>
        </div>
        {this.renderMenu()}
      </div>
    )
  }

  renderMenu () {
    const links1 = this.props.mainMenu.items
    const links2 = this.props.topMenu.items
    const { langs } = this.props.locales

    return (
      <div ref='menu' className='mobile-menu--hovering-menu' onScroll={this.handleMenuScroll}>
        <span className='mobile-menu--hovering-menu--close' onClick={this.handleClickCloseIcon}><CloseIcon /></span>
        <div className='mobile-menu--hovering-menu--langs'>
          <span className='active' onClick={this.handleLangClick}>{langs[0].title} <ArrowDownIcon /></span>
          {this.renderLanguages(langs)}
        </div>
        <ul>
          {links1.map(this.renderMenuItem)}
        </ul>
        <hr />
        <ul>
          {links2.map(this.renderMenuItem)}
        </ul>
        <hr />
        <ul>
          <li><a href='#'>Кабинет</a></li>
        </ul>
        <div className='mobile-menu--hovering-menu--phone'>
          +7 (495) 363-60-45
        </div>
      </div>
    )
  }

  renderLanguages () {
    const { selectedLocale, langs } = this.props.locales

    return (
      <ul ref='langList'>
        {Object.keys(langs).map((k) => {
          const item = langs[k]
          let innerTag = (<a href='#'>{item.title}</a>)

          if (item.id === selectedLocale) {
            innerTag = (<span>{item.title}</span>)
          }

          return (<li key={k} className='main-menu--lang' onClick={this.changeLocale.bind(this, item.id)}>{innerTag}</li>)
        })}
      </ul>
    )
  }

  changeLocale (id) {
    this.props.setLocale(id)
  }

  renderMenuItem (item, index) {
    return (
      <li key={index}>
        <a href={item.url}>{item.title}</a>
      </li>
    )
  }

  handleClickMenuIcon (e) {
    this.refs.menu.style.display = 'block'
  }

  handleClickCloseIcon (e) {
    this.refs.menu.style.display = 'none'
    document.getElementsByTagName('body')[0].style.overflowY = 'auto'
  }

  handleMenuScroll (e) {
    document.getElementsByTagName('body')[0].style.overflowY = 'hidden'
  }

  handleLangClick () {
    const display = this.refs.langList.style.display

    if (display !== 'block') {
      this.refs.langList.style.display = 'block'
    } else {
      this.refs.langList.style.display = 'none'
    }
  }
}

function mapStateToProps (state) {
  return {
    locales: state.locales,
    mainMenu: state.menus.mainMenu,
    topMenu: state.menus.topMenu
  }
}

export default connect(mapStateToProps, {
  setLocale
})(MobileMenu)
