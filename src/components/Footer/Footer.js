import React, { PropTypes } from 'react'
import { connect } from 'react-redux'
import MediaQuery from 'react-responsive'

import { FbIcon, VkIcon, PinIcon, InstIcon, BeIcon, ReIcon, BallIcon } from '../Common/IconsCollection'

export class Footer extends React.Component {
  static propTypes = {
    mainMenu: PropTypes.object.isRequired,
    topMenu: PropTypes.object.isRequired,
    i18n: PropTypes.object,
    params: PropTypes.object
  };

  constructor (...args) {
    super(...args)

    this.firstLinkList = this.props.mainMenu.items
    this.secondLinkList = this.props.topMenu.items
  }

  render () {
    return (
      <div>
        <MediaQuery query='(max-width: 768px)'>
          {this.renderMobileFooter()}
        </MediaQuery>
        <MediaQuery query='(min-width: 769px) and (max-width: 1023px)'>
          {this.renderTabletFooter()}
        </MediaQuery>
        <MediaQuery query='(min-width: 1024px)'>
          {this.renderDesktopFooter()}
        </MediaQuery>
      </div>
    )
  }
  renderTabletFooter () {
    const { i18n } = this.props

    return (
      <div className='page-footer'>
        <div className='page-footer--row'>
          {this.renderList(this.firstLinkList, i18n.mainMenu)}
          {this.renderList(this.secondLinkList, i18n.topMenu)}

          <div className='page-footer--block'>
            <ul className='page-footer--social-links'>
              <li className='page-footer--social-link'>
                <a href='#'>
                  <FbIcon />
                </a>
              </li>
              <li className='page-footer--social-link'>
                <a href='#'>
                  <VkIcon />
                </a>
              </li>
              <li className='page-footer--social-link'>
                <a href='#'>
                  <PinIcon />
                </a>
              </li>
              <li className='page-footer--social-link'>
                <a href='#'>
                  <InstIcon />
                </a>
              </li>
            </ul>
            <ul className='page-footer--app-items'>
              <li className='page-footer--app-item'>
                <a href='#' className='page-footer--app-item--apple'></a>
              </li>
            </ul>
          </div>

          <div className='page-footer--block'>
            <ul className='page-footer--design-links'>
              <li className='page-footer--design-link'>
                <a href='#'>
                  <BeIcon />
                </a>
              </li>
              <li className='page-footer--design-link page-footer--design-link-re'>
                <a href='#'>
                  <ReIcon />
                </a>
              </li>
              <li className='page-footer--design-link'>
                <a href='#'>
                  <BallIcon />
                </a>
              </li>
            </ul>

            <ul className='page-footer--app-items'>
              <li className='page-footer--app-item'>
                <a href='#' className='page-footer--app-item--google'></a>
              </li>
            </ul>
          </div>

        </div>

        <hr className='page-footer--separator' />

        <div className='page-footer--row'>
          <div className='page-footer--block'>
            <p>Все права защищены</p>
            <p>&copy;CatsFollowMe 2015</p>
          </div>
          <div className='page-footer--block'>
            Сделано в <a href='http://zendesign.ru/ru/'>Дзен Дизайн</a>
          </div>
          <div className='page-footer--block'>
            <a href='#'>Иллюстраторам</a>
          </div>
          <div className='page-footer--block'>
            <a href='mailto:inspiration@catsfollow.me'>inspiration@catsfollow.me</a>
          </div>
        </div>
      </div>
    )
  }

  renderDesktopFooter () {
    const { i18n } = this.props

    return (
      <div className='page-footer'>
        <div className='page-footer--row'>
          {this.renderList(this.firstLinkList, i18n.mainMenu)}
          {this.renderList(this.secondLinkList, i18n.topMenu)}

          <div className='page-footer--block'>
            <ul className='page-footer--social-links'>
              <li className='page-footer--social-link'>
                <a href='#'>
                  <FbIcon />
                </a>
              </li>
              <li className='page-footer--social-link'>
                <a href='#'>
                  <VkIcon />
                </a>
              </li>
              <li className='page-footer--social-link'>
                <a href='#'>
                  <PinIcon />
                </a>
              </li>
              <li className='page-footer--social-link'>
                <a href='#'>
                  <InstIcon />
                </a>
              </li>
            </ul>

            <ul className='page-footer--payment-items'>
              <li className='page-footer--payment-item page-footer--payment-item--visa'>
                <a href='#'><img src='/images/visa.png' /></a>
              </li>
              <li className='page-footer--payment-item page-footer--payment-item--mc'>
                <a href='#'><img src='/images/mc.png' /></a>
              </li>
              <li className='page-footer--payment-item page-footer--payment-item--paypal'>
                <a href='#'><img src='/images/paypal.png' /></a>
              </li>
            </ul>
          </div>

          <div className='page-footer--block'>
            <ul className='page-footer--design-links'>
              <li className='page-footer--design-link'>
                <a href='#'>
                  <BeIcon />
                </a>
              </li>
              <li className='page-footer--design-link page-footer--design-link-re'>
                <a href='#'>
                  <ReIcon />
                </a>
              </li>
              <li className='page-footer--design-link'>
                <a href='#'>
                  <BallIcon />
                </a>
              </li>
            </ul>

            <ul className='page-footer--app-items'>
              <li className='page-footer--app-item'>
                <a href='#' className='page-footer--app-item--apple'></a>
              </li>
              <li className='page-footer--app-item'>
                <a href='#' className='page-footer--app-item--google'></a>
              </li>
            </ul>
          </div>

        </div>

        <hr className='page-footer--separator' />

        <div className='page-footer--row'>
          <div className='page-footer--block'>
            <p>{i18n.rights}</p>
            <p>&copy;CatsFollowMe 2015</p>
          </div>
          <div className='page-footer--block' dangerouslySetInnerHTML={{ __html: i18n.madeIn }}></div>
          <div className='page-footer--block'>
            <a href='#'>Иллюстраторам</a>
          </div>
          <div className='page-footer--block'>
            <a href='mailto:inspiration@catsfollow.me'>inspiration@catsfollow.me</a>
          </div>
        </div>
      </div>
    )
  }

  renderMobileFooter () {
    const { i18n } = this.props

    return (
      <div className='page-footer'>
        <div className='page-footer--row'>
            <ul className='page-footer--social-links'>
              <li className='page-footer--social-link'>
                <a href='#'>
                  <FbIcon />
                </a>
              </li>
              <li className='page-footer--social-link'>
                <a href='#'>
                  <VkIcon />
                </a>
              </li>
              <li className='page-footer--social-link'>
                <a href='#'>
                  <PinIcon />
                </a>
              </li>
              <li className='page-footer--social-link'>
                <a href='#'>
                  <InstIcon />
                </a>
              </li>
            </ul>

            <ul className='page-footer--design-links'>
              <li className='page-footer--design-link'>
                <a href='#'>
                  <BeIcon />
                </a>
              </li>
              <li className='page-footer--design-link page-footer--design-link-re'>
                <a href='#'>
                  <ReIcon />
                </a>
              </li>
              <li className='page-footer--design-link page-footer--design-link-dribble'>
                <a href='#'>
                  <BallIcon />
                </a>
              </li>
            </ul>

            <hr className='page-footer--separator' />

            <div className='page-footer--row'>
              <div className=''>
                <p>{i18n.rights} &copy; CatsFollowMe 2015</p>
              </div>
              <div className='' dangerouslySetInnerHTML={{ __html: i18n.madeIn }}></div>
            </div>
        </div>
      </div>
    )
  }

  renderList (list, i18n) {
    return (
      <ul className='page-footer--block'>
        {list.map((item, k) => {
          return (<li key={k}><a href={`/${this.props.params.lang}/${this.props.params.style}/${item.url}`}>{i18n[item.url]}</a></li>)
        })}
      </ul>
    )
  }
}

function mapStateToProps (state) {
  return {
    mainMenu: state.menus.mainMenu,
    topMenu: state.menus.topMenu
  }
}

export default connect(mapStateToProps)(Footer)
