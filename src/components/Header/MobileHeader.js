import React, { PropTypes } from 'react'

import StyleSwitcher from '../Common/StyleSwitcher'
import MobileMenu from '../MainMenu/MobileMenu'
import { BasketIcon } from '../Common/IconsCollection'

export class MobileHeader extends React.Component {
  static propTypes = {
    transparent: PropTypes.bool,
    absolute: PropTypes.bool
  };

  render () {
    let classNames = ['mobile-header']

    if (this.props.transparent) {
      classNames.push('mobile-header--transparent')
    }
    if (this.props.absolute) {
      classNames.push('mobile-header--absolute')
    }

    return (
      <div className={classNames.join(' ')}>
        <div className='mobile-header--basket'>
          <BasketIcon />
        </div>
        <StyleSwitcher style={'light'}
                       activeElementIndex={0}
                       elWidth={80}>
          <a href='#'>Модерн</a>
          <a href='#'>Винтаж</a>
        </StyleSwitcher>
        <MobileMenu />
      </div>
    )
  }
}

export default MobileHeader
