import React, { PropTypes } from 'react'
import MediaQuery from 'react-responsive'

import Header from 'components/Header/Header'
import MobileHeader from 'components/Header/MobileHeader'

export class InnerHeader extends React.Component {
  static propTypes = {
    isCollectionView: PropTypes.bool,
    isHomeView: PropTypes.bool,
    i18n: PropTypes.object
  };

  render () {
    return (
      <div>
        <MediaQuery query='(max-width: 680px)'>
          <MobileHeader transparent absolute={this.props.isCollectionView} {...this.props} />
        </MediaQuery>
        <MediaQuery query='(min-width: 681px) and (max-width: 960px)'>
          <MobileHeader transparent={this.props.isHomeView} {...this.props} />
        </MediaQuery>
        <MediaQuery query='(min-width: 961px)'>
          <Header transparent={this.props.isHomeView} {...this.props} />
        </MediaQuery>
      </div>
    )
  }
}

export default InnerHeader
