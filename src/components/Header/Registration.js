import React, { PropTypes } from 'react'
import { connect } from 'react-redux'

import { setRegistrationForm, REGISTRATION_FORM, LOGIN_FORM, FORGET_PASS_FORM } from '../../redux/modules/registration'

import StyleSwitcher from '../Common/StyleSwitcher'
import { VkIcon, FbIcon, TwIcon } from '../Common/IconsCollection'

export class Registration extends React.Component {
  static propTypes = {
    registration: PropTypes.object.isRequired,
    setRegistrationForm: PropTypes.func.isRequired
  };

  constructor (...args) {
    super(...args)

    this.renderLoginForm = this.renderLoginForm.bind(this)
    this.renderForgetPassForm = this.renderForgetPassForm.bind(this)
    this.changeForm = this.changeForm.bind(this)
  }

  render () {
    const formToRender = {
      [REGISTRATION_FORM]: this.renderRegistrationForm,
      [LOGIN_FORM]: this.renderLoginForm,
      [FORGET_PASS_FORM]: this.renderForgetPassForm
    }

    const switcherIndexes = {
      [REGISTRATION_FORM]: 0,
      [LOGIN_FORM]: 1,
      [FORGET_PASS_FORM]: 1
    }

    return (
      <div className='registration'>
        <div className='registration-block'>
          <StyleSwitcher activeElementIndex={switcherIndexes[this.props.registration.shownForm]}
                         elWidth={110}>
            <a href='#' onClick={this.changeForm.bind(this, REGISTRATION_FORM)}>Регистрация</a>
            <a href='#' onClick={this.changeForm.bind(this, LOGIN_FORM)}>Вход</a>
          </StyleSwitcher>
        </div>
        <div className='registration-block registration-block--links'>
          Войти через социальные сети: {this.renderSocialLinks()}
        </div>

        {formToRender[this.props.registration.shownForm]()}
      </div>
    )
  }

  renderLoginForm () {
    return (
      <form autoComplete='off' className='registration--form registration--login-form'>
        <div className='registration-block'>
          <div className='text-input'>
            <input type='text' placeholder='Электронная почта' />
          </div>
        </div>
        <div className='registration-block'>
          <div className='text-input'>
            <input type='password' placeholder='Пароль' />
          </div>
        </div>
        <div className='registration-block registration-block-button'>
          <button className='button'>Войти</button>
        </div>
        <div className='registration-block'>
          <a href='#' onClick={this.changeForm.bind(this, FORGET_PASS_FORM)}>Забыли пароль?</a>
        </div>
      </form>
    )
  }

  renderRegistrationForm () {
    return (
      <form autoComplete='off' className='registration--form'>
        <div className='registration-block'>
          <div className='text-input'>
            <input type='text' placeholder='Электронная почта' />
          </div>
        </div>
        <div className='registration-block'>
          <div className='text-input'>
            <input type='password' placeholder='Пароль' />
          </div>
        </div>
        <div className='registration-block'>
          <button className='button'>Зарегистрироваться</button>
        </div>
      </form>
    )
  }

  renderForgetPassForm () {
    return (
      <form autoComplete='off' className='registration--form registration--forget-form'>
        <div className='registration-block'>
          <div className='text-input'>
            <input type='text' placeholder='Электронная почта' />
          </div>
        </div>
        <div className='registration-block registration-block-button'>
          <button className='button'>Отправить</button>
        </div>
        <div className='registration-block'>
          <a href='#' onClick={this.changeForm.bind(this, LOGIN_FORM)}>Вернуться назад</a>
        </div>
      </form>
    )
  }

  renderSocialLinks () {
    const links = [
      {
        title: 'vk.com',
        className: 'vk'
      },
      {
        title: 'facebook',
        className: 'fb'
      },
      {
        title: 'twitter',
        className: 'tw'
      }
    ]
    const icons = {
      'vk': <VkIcon />,
      'fb': <FbIcon />,
      'tw': <TwIcon />
    }

    return links.map((l, k) => (<a key={k} href='#' className={l.className}>{icons[l.className]}{l.title}</a>))
  }

  changeForm (form_type, e) {
    this.props.setRegistrationForm(form_type)
  }
}

function mapStateToProps (state) {
  return {
    registration: state.registration
  }
}

export default connect(mapStateToProps, {
  setRegistrationForm
})(Registration)
