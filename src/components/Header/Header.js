import React, { PropTypes } from 'react'
import { connect } from 'react-redux'
import MediaQuery from 'react-responsive'

import { toggleRegistrationBlock } from 'redux/modules/registration'

import { MobileMenuIcon } from 'components/Common/IconsCollection'

export class Header extends React.Component {
  static propTypes = {
    transparent: PropTypes.bool,
    topMenu: PropTypes.object.isRequired,
    toggleRegistrationBlock: PropTypes.func.isRequired,
    i18n: PropTypes.object,
    params: PropTypes.object
  };

  constructor (...args) {
    super(...args)

    this.renderLink = this.renderLink.bind(this)
    this.handleCabinetClick = this.handleCabinetClick.bind(this)
  }

  render () {
    const links = this.props.topMenu.items

    let classNames = ['page-header']

    if (this.props.transparent) {
      classNames.push('page-header--transparent')
    }

    return (
      <div className={classNames.join(' ')}>
        {links.map(this.renderLink)}
        <div className='page-header--right-block'>
        {this.renderPhone()}
        <MediaQuery query='(min-width: 961px) and (max-width: 1200px)'>
         <div className='burger-icon' onClick={this.showMainMenu}><MobileMenuIcon /></div>
        </MediaQuery>
        {this.renderCabinet()}
        </div>
      </div>
    )
  }

  renderLink (item, k) {
    const { i18n } = this.props

    return (<a key={k} href={`/${this.props.params.lang}/${this.props.params.style}/${item.url}`} className='page-header--item'>{i18n.topMenu[item.url]}</a>)
  }

  renderPhone () {
    return (<span className='page-header--phone'>+7 (495) 363-60-45</span>)
  }

  renderCabinet () {
    const { i18n } = this.props

    return (<a href='#'
               className='page-header--item page-header--cabinet'
               onClick={this.handleCabinetClick}>
                {i18n.cabinet}
            </a>)
  }

  handleCabinetClick () {
    this.props.toggleRegistrationBlock()
  }

  showMainMenu () {
    document.getElementsByTagName('body')[0].classList.toggle('opened-menu')
  }
}

function mapStateToProps (state) {
  return {
    topMenu: state.menus.topMenu
  }
}

export default connect(mapStateToProps, {
  toggleRegistrationBlock
})(Header)
