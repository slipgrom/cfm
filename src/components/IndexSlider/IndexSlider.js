import React, { PropTypes, Component } from 'react'

export class IndexSlider extends Component {
  static propTypes = {
    items: PropTypes.array.isRequired
  };

  constructor (...args) {
    super(...args)

    this.renderIcon = this.renderIcon.bind(this)
    this.renderSlide = this.renderSlide.bind(this)
  }
  render () {
    const { items } = this.props

    return (
      <div className='index-slider'>
        <div className='index-slider--icons'>
          {items.map(this.renderIcon)}
        </div>
        <div className='index-slider--slides'>
          {items.map(this.renderSlide)}
        </div>
      </div>
    )
  }

  renderIcon (item, k) {
    const style = {
      background: `url(${item.iconUrl})`
    }

    return (
      <div key={k} className='index-slider--icon' style={style}>
        <svg width='44' height='44'>
          <circle cx='21.5' cy='21.5' r='20' strokeWidth='2' stroke='#fff' fill='none' />
        </svg>
      </div>
    )
  }

  // <div key={k} className='index-slider--slide'>{item.slideUrl}</div>
  renderSlide (item, k) {
    return (
      null
    )
  }
}

export default IndexSlider
