import React from 'react'

import { LeftArrow, RightArrow } from '../Common/IconsCollection'

export class PageNav extends React.Component {
  constructor (...args) {
    super(...args)

    this.renderPageNumber = this.renderPageNumber.bind(this)
  }

  render () {
    const pages = [1, 2, 3]

    return (
      <div className='page-nav'>
        <a href='#' className='page-nav--item page-nav--prev'><LeftArrow /></a>
        <span className='page-nav--pages'>
          {pages.map(this.renderPageNumber)}
        </span>
        <a href='#' className='page-nav--item page-nav--next'><RightArrow /></a>
      </div>
    )
  }

  renderPageNumber (page, k) {
    const activePage = 0
    if (activePage === k) {
      return (<span key={k} className='page-nav--item active'>{page}</span>)
    }
    return (<a key={k} href='#' className='page-nav--item'>{page}</a>)
  }
}

export default PageNav
