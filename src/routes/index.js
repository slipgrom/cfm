import React from 'react'
import { Route, IndexRoute, Redirect } from 'react-router'

// NOTE: here we're making use of the `resolve.root` configuration
// option in webpack, which allows us to specify import paths as if
// they were from the root of the ~/src directory. This makes it
// very easy to navigate to files regardless of how deeply nested
// your current file is.
import CoreLayout from 'layouts/CoreLayout/CoreLayout'
import HomeView from 'views/HomeView/HomeView'

import AboutView from 'views/AboutView/AboutView'
import FeedbackView from 'views/FeedbackView/FeedbackView'
import DeliveryView from 'views/DeliveryView/DeliveryView'

import CollectionView from 'views/CollectionView/CollectionView'
import CollectionsView from 'views/CollectionsView/CollectionsView'
import TshirtsView from 'views/TshirtsView/TshirtsView'
import WallpapersView from 'views/WallpapersView/WallpapersView'
import PostersView from 'views/PostersView/PostersView'

import BasketView from 'views/BasketView/BasketView'

import NotFoundView from 'views/NotFoundView/NotFoundView'

import TestView from 'views/TestView/TestView'

export default (store) => (
  <Route path='/'>
    <Route path=':lang/:style' component={CoreLayout}>
    <IndexRoute component={HomeView} />
    <Route path='about' component={AboutView} />
    <Route path='feedback' component={FeedbackView} />

    <Route path='collections/:id'>
        <Route path=':type' component={CollectionView} />
    </Route>
    <Route path='collections' component={CollectionsView} />
    <Route path='tshirts' component={TshirtsView} />
    <Route path='wallpapers' component={WallpapersView} />
    <Route path='posters' component={PostersView} />

    <Route path='basket'>
        <IndexRoute component={BasketView} />
        <Route path='delivery' component={DeliveryView} />
    </Route>
    <Route path='/404' component={NotFoundView} />

    <Redirect from='*' to='/404' />
    </Route>
    <Route path='/testpage' component={TestView} />
  </Route>
)
