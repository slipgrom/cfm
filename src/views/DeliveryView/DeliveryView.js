import React, { PropTypes } from 'react'
import { connect } from 'react-redux'

import IosSwitcher from 'components/Common/IosSwitcher'
import Select from 'react-select'
import { RubleSign } from 'components/Common/IconsCollection'

// import '../../node_modules/react-select/react-select.min.css'

import { changeDeliveryType,
         toggleRegister,
         DELIVERY_TYPE_EMS,
         DELIVERY_TYPE_COURIER } from 'redux/modules/basket'

export class DeliveryView extends React.Component {
  static propTypes = {
    basket: PropTypes.object.isRequired,
    toggleRegister: PropTypes.func.isRequired,
    changeDeliveryType: PropTypes.func.isRequired
  };

  constructor (...args) {
    super(...args)

    this.renderDeliveryTotal = this.renderDeliveryTotal.bind(this)
    this.handleRegisterClick = this.handleRegisterClick.bind(this)
  }

  render () {
    const { basket: { register, currentDeliveryType } } = this.props

    return (
      <div className='delivery-view'>
        <div className='content-header content-header--basket'>
          <div className='title'>Оформление заказа</div>
        </div>

        <div className='feedback-content'>
          <div className='feedback-form'>
            <form action='/' className='form'>
              <div className='feedback-form--left-block'>
                <h2>Личные данные</h2>
                <div className='form-item'>
                  <label>Имя и фамилия</label>
                  <div className='text-input'>
                    <input type='text' name='feedback[name]' />
                  </div>
                </div>
                <div className='form-item'>
                  <label>Телефон</label>
                  <div className='text-input'>
                    <input type='text' name='feedback[phone]' />
                  </div>
                </div>
                <div className='form-item'>
                  <label>Электронная почта</label>
                  <div className='text-input'>
                    <input type='text' name='feedback[email]' />
                  </div>
                </div>
                <div className='form-item delivery-registration'>
                  Регистрация для будущих покупок <IosSwitcher active={register} onClick={this.handleRegisterClick}/>
                </div>
                {register && <div className='form-item'>
                  <label>Пароль</label>
                  <div className='text-input'>
                    <input type='password' name='feedback[email]' />
                  </div>
                </div>}
              </div>

              <div className='feedback-form--right-block'>
                <h2>Адрес доставки</h2>
                <div className='form-item'>
                  <label>Страна и город</label>
                  <div>
                    <Select name='feedback[email]'
                            placeholder=''
                            options={[{value: 1, label: 'Сотрудничество'}, {value: 2, label: 'Сотрудничество 2'}]} />
                  </div>
                </div>
                <div className='form-item'>
                  <label>Улица, дом, строение, квартира</label>
                  <div className='text-input'>
                    <input type='text' name='feedback[email]' />
                  </div>
                </div>
                <div className='form-item'>
                  <label>Способ доставки</label>
                  <div className='delivery-input'>
                    <div className='delivery-left'>
                      <input type='radio'
                             name='delivery[type]'
                             onChange={this.handleDeliveryTypeCheck.bind(this, DELIVERY_TYPE_EMS)}
                             checked={currentDeliveryType === DELIVERY_TYPE_EMS} />
                      <label>EMS</label>
                      <h5>Способ доставки</h5>
                      <h6>350 р.</h6>
                      <h5>Срок доставки</h5>
                      <h6>5 дней</h6>
                      <p>Если нужно не срочно, а сэкономить.</p>
                    </div>
                    <div className='delivery-right'>
                      <input type='radio'
                             name='delivery[type]'
                             onChange={this.handleDeliveryTypeCheck.bind(this, DELIVERY_TYPE_COURIER)}
                             checked={currentDeliveryType === DELIVERY_TYPE_COURIER} />
                      <label>Курьером</label>
                      <h5>Способ доставки</h5>
                      <h6>500 р.</h6>
                      <h5>Срок доставки</h5>
                      <h6>1 день</h6>
                      <p>Если нужно срочно.</p>
                    </div>
                  </div>
                </div>
              </div>
              <div className='feedback-form--left-block'>
                <h2>Способ оплаты</h2>
                <div className='form-item'>
                 - Банковская карта
                 - Яндекс касса
                </div>
              </div>
            </form>
          </div>
          <div className='delivery-total-block'>
            <a href='#' className='rounded-button'>Купить</a>
            <h3>Итого с учетом доставки {this.renderDeliveryTotal()}</h3>
          </div>
        </div>
      </div>
    )
  }

  get total () {
    const { basket: { items } } = this.props
    let total = 0

    items.map((item) => {
      total += item.price * item.count
    })

    return total
  }

  get deliveryPrice () {
    const { basket: { deliveryTypePrices, currentDeliveryType } } = this.props

    return deliveryTypePrices[currentDeliveryType]
  }

  renderDeliveryTotal () {
    return (<span className='price'>{this.total + this.deliveryPrice} <RubleSign /></span>)
  }

  handleRegisterClick (e) {
    e.preventDefault()
    this.props.toggleRegister()
    return false
  }

  handleDeliveryTypeCheck (type) {
    this.props.changeDeliveryType(type)
  }
}

function mapStateToProps (state) {
  return {
    basket: state.basket
  }
}

export default connect(mapStateToProps, {
  toggleRegister,
  changeDeliveryType
})(DeliveryView)
