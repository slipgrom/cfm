import React from 'react'
import ScrollMagic from 'scrollmagic'

export class HomeView extends React.Component {
  static propTypes = {
  };

  constructor (...args) {
    super(...args)

    this.state = {
      activePage: 0,
      animating: false
    }

    this.handleScroll = this.handleScroll.bind(this)
    this.updateSizes = this.updateSizes.bind(this)

    this.animatePage = this.animatePage.bind(this)
  }

  componentDidMount () {
    window.addEventListener('wheel', this.handleScroll)
    window.addEventListener('resize', this.updateSizes)

    this.pages = this.refs.container.querySelectorAll('.page')

    this.controller = new ScrollMagic.Controller({
      globalSceneOptions: {
        triggerHook: 'onLeave'
      }
    })

    this.updateSizes()
  }

  componentDidUpdate () {
    console.log(this.state.activePage)
    this.animatePage()
  }

  componentWillUnmount () {
    window.removeEventListener('wheel', this.handleScroll)
    window.removeEventListener('resize', this.updateSizes)
  }

  shouldComponentUpdate (nextProps, nextState) {
    return this.state.activePage !== nextState.activePage
  }

  render () {
    return (
      <div ref='container' className='fixed-container'>
        <div className='page page1'>1<span className='indicator'>end</span></div>
        <div className='page page2'>2<span className='indicator'>end</span></div>
        <div className='page page3'>3<span className='indicator'>end</span></div>
      </div>
    )
  }

  handleScroll (e) {
    if (!this.state.animating) {
      if (e.deltaY > 20) {
        this.setState({
          animating: true,
          activePage: Math.min(this.state.activePage + 1, this.pages.length - 1)
        })
      } else {
        if (e.deltaY < -20) {
          this.setState({
            animating: true,
            activePage: Math.max(this.state.activePage - 1, 0)
          })
        }
      }
    }
  }

  updateSizes () {
    const slide1 = this.refs.container.querySelector('.page1')
    const slide2 = this.refs.container.querySelector('.page2')
    const slide3 = this.refs.container.querySelector('.page3')
    new ScrollMagic.Scene({
      triggerElement: slide1,
      offset: 300
    })
    .setPin(slide1)
    .addTo(this.controller)

    new ScrollMagic.Scene({
      triggerElement: slide2
    })
    .setPin(slide2)
    .addTo(this.controller)
    new ScrollMagic.Scene({
      triggerElement: slide3
    })
    .addTo(this.controller)

    // this.controller.scrollTo(scene2)
    // _.forEach(this.pages, (slide, k) => {
    //   // slide.style.height = `${window.innerHeight}px`
    //   // slide.style.top = `${window.innerHeight * k}px`

    //   switch (k) {
    //     case 0:

    //       break
    //     case 1:
    //       new ScrollMagic.Scene({
    //         triggerElement: slide
    //       })
    //       // .setPin(slide)
    //       .addTo(this.controller)
    //       break
    //     case 2:
    //       new ScrollMagic.Scene({
    //         triggerElement: slide
    //       })
    //       // .setPin(slide)
    //       .addTo(this.controller)
    //       break
    //   }
    // })

    // this.refs.container.style.height = `${window.innerHeight * this.pages.length}px`
  }

  animatePage () {
    // Velocity(
    //   this.refs.container.querySelector(`.page${this.state.activePage + 1}`),
    //   'scroll',
    //   {
    //     duration: 500,
    //     complete: () => {
    //       this.setState({
    //         animating: false
    //       })
    //     }
    //   })
  }
}

export default HomeView
