import React, { PropTypes } from 'react'
import { connect } from 'react-redux'

import Collection from 'components/Collection/Collection'
import Announcement from 'components/Announcement/Announcement'

const mapStateToProps = (state) => ({
  collections: state.collections
})

export class CollectionView extends React.Component {
  static propTypes = {
    collections: PropTypes.array.isRequired,
    params: PropTypes.object.isRequired
  };

  render () {
    const { collections, params: { style, lang } } = this.props

    return (
      <div>
        <div className='content-header content-header--collections'>
          <div className='title'>Коллекции</div>
        </div>
        {collections && collections.map((collection, k) => {
          return <Collection key={k} collection={collection} style={style} lang={lang} />
        })}
        <Announcement />
      </div>
    )
  }
}

export default connect(mapStateToProps)(CollectionView)
