import React, { PropTypes } from 'react'
import { connect } from 'react-redux'

const mapStateToProps = (state, ownProps) => {
  const locale = state.locales.langs[state.locales.selectedLocale]
  const i18n = state.i18n[locale.title]

  return {
    i18n: i18n
  }
}

export class SaleBlock extends React.Component {
  static propTypes = {
    i18n: PropTypes.object.isRequired
  };

  render () {
    const { i18n } = this.props

    return (
      <div className='sale-container'>
        <span className='sale-percents'>15%</span>
        <span className='sep'></span>
        <span className='text'>{i18n.collection.sale}</span>
        <span className='sep'></span>
        <span className='types'></span>
      </div>
    )
  }
}

export default connect(mapStateToProps)(SaleBlock)
