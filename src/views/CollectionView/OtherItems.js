import React, { PropTypes } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router'
import _ from 'lodash'

import ItemImage from 'components/Collection/ItemImage'

import {
  TSHIRT,
  WALLPAPER,
  POSTER
} from 'redux/modules/collections'

const mapStateToProps = (state, ownProps) => {
  const locale = state.locales.langs[state.locales.selectedLocale]
  const i18n = state.i18n[locale.title]

  return {
    i18n: i18n
  }
}

export class OtherItems extends React.Component {
  static propTypes = {
    collection: PropTypes.object.isRequired,
    currentType: PropTypes.string.isRequired,
    i18n: PropTypes.object.isRequired,
    style: PropTypes.string.isRequired
  };

  render () {
    const { currentType, collection, i18n, style } = this.props
    const types = [TSHIRT, WALLPAPER, POSTER]

    _.pull(types, currentType)

    return (
      <div className='other-types'>
        {types.map((t, k) => {
          const c = `other-type other-type-${_.toLower(t)}`
          const url = `/ru/vintage/collections/${collection.id}/${_.toLower(t)}`
          const image = (<ItemImage collectionId={collection.id} type={t} style={style} />)

          return (
            <Link to={url} key={k} className={c}>
              {i18n.collection[_.toLower(t)]}
              {image}
            </Link>
          )
        })}
      </div>
    )
  }
}

export default connect(mapStateToProps)(OtherItems)
