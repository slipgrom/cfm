import React, { PropTypes, Component } from 'react'

import { CloseIcon } from 'components/Common/IconsCollection'

export class PanelInfo extends Component {
  static propTypes = {
    i18n: PropTypes.object.isRequired
  };

  constructor (...args) {
    super(args)

    this.state = {
      opened: false
    }

    this.setHeight = this.setHeight.bind(this)
    this.handleInfoClick = this.handleInfoClick.bind(this)
    this.handleCloseClick = this.handleCloseClick.bind(this)
    this.renderText = this.renderText.bind(this)
  }

  componentDidMount () {
    this.setHeight()
  }
  componentDidUpdate () {
    this.setHeight()
  }

  render () {
    const { opened } = this.state
    const classNames = ['panel__info-sign']

    if (opened) {
      classNames.push('panel__info-sign_opened')
    }
    return (
      <div ref='sign' className={classNames.join(' ')} onClick={this.handleInfoClick}>
        {opened && this.renderTable()}
        {this.renderText()}
      </div>
    )
  }

  setHeight () {
    if (this.state.opened) {
      const panel = document.querySelector('.panel')
      const width = panel.offsetWidth
      const height = window.innerHeight - panel.offsetHeight

      this.refs.table.style.width = `${width}px`
      this.refs.table.style.height = `${height}px`

      const { sign, arrow } = this.refs
      arrow.style.left = `${sign.offsetLeft + sign.offsetWidth/2 - arrow.offsetWidth/2}px`
    }
  }

  handleInfoClick (e) {
    if (!this.refs.table || (this.refs.table && !this.refs.table.contains(e.target))) {
      this.setState({
        opened: !this.state.opened
      })
    }
  }

  handleCloseClick (e) {
    e.preventDefault()
    this.setState({
      opened: false
    })
  }

  renderTable () {
    return (
      <div className='panel__info-sign__table table' ref='table'>
        <i className='table__arrow' ref='arrow'></i>
        <i className='table__close' onClick={this.handleCloseClick}><CloseIcon /></i>
        <div className='table__content'>
          {this.renderMenTable()}

          <div className='table__content__description'>
            <div>
              <img src='/images/info-table-img.png' />
            </div>
            <div className='center'>
              <p>Как не ошибиться с размером? Все просто. Возьмите свою самую любимую футболку</p>
              <ul>
                <li><strong>1.</strong> Измерьте ширину, см. (от подмышки до подмышки)</li>
                <li><strong>2.</strong> Измерьте длину, см. (от верхней точки плеча до низа)</li>
                <li><strong>3.</strong> Сверьте данные с таблицей</li>
              </ul>
            </div>

            <div>
              <p>
                Если ваши размеры отличаются от цифр представленных в таблице, то решающим при
                определении размера одежды должен являться больший параметр. Свободная вещь всегда смотрится
                более гармонично, чем плотно облегающая.
              </p>
              <p>
                Размер не подошел? Не переживайте! Простая и бесплатная процедура возврата доступна в течение
                30 дней. Ознакомьтесь с условиями возврата, чтобы узнать больше.
              </p>
            </div>
          </div>
        </div>
      </div>
    )
  }

  renderText () {
    return (
      <span className='panel__info-sign__text'>{this.props.i18n.common.infoTable}</span>
    )
  }

  renderMenTable () {
    return (
      <table>
        <tbody>
          <tr>
            <th></th>
            <th>S</th>
            <th>M</th>
            <th>L</th>
            <th>XL</th>
            <th>2XL</th>
          </tr>
          <tr>
            <td>Размер RUS</td>
            <td>46-48</td>
            <td>48-50</td>
            <td>50-52</td>
            <td>52-54</td>
            <td>54-56</td>
          </tr>
          <tr>
            <td>Обхват груди, см.</td>
            <td>100</td>
            <td>104</td>
            <td>108</td>
            <td>112</td>
            <td>116</td>
          </tr>
          <tr>
            <td>Ширина, см.</td>
            <td>48</td>
            <td>50</td>
            <td>52</td>
            <td>54</td>
            <td>56</td>
          </tr>
          <tr>
            <td>Длина, см.</td>
            <td>70</td>
            <td>72</td>
            <td>74</td>
            <td>74 / 76</td>
            <td>74 / 78</td>
          </tr>
        </tbody>
      </table>
    )
  }

  renderWomenTable () {
    return (
      <table>
        <tbody>
          <tr>
          <td></td>
          <td>XS</td>
          <td>S</td>
          <td>M</td>
          <td>L</td>
          <td>XL</td>
          <td>2XL</td>
          </tr>
          <tr>
          <td>Размер RUS</td>
          <td>38-40</td>
          <td>40-42</td>
          <td>42-44</td>
          <td>44-46</td>
          <td>46-48</td>
          <td>48-50</td>
          </tr>
          <tr>
          <td>ширина,&nbsp;см.</td>
          <td>40</td>
          <td>42</td>
          <td>44</td>
          <td>46</td>
          <td>48</td>
          <td>50</td>
          </tr>
          <tr>
          <td>длина,&nbsp;см.</td>
          <td>61</td>
          <td>62</td>
          <td>64</td>
          <td>64</td>
          <td>64 / 66</td>
          <td>64 / 68</td>
          </tr>
          <tr>
          <td>обхват груди,&nbsp;см.</td>
          <td>76-80</td>
          <td>80-84</td>
          <td>84-88</td>
          <td>88-92</td>
          <td>92-96</td>
          <td>96-100</td>
          </tr>
        </tbody>
      </table>
    )
  }
}

export default PanelInfo
