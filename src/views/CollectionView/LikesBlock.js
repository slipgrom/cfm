import React, { PropTypes } from 'react'

import { LikeButtonCat } from 'components/Common/IconsCollection'

export class LikesBlock extends React.Component {
  static propTypes = {
    likes: PropTypes.number.isRequired,
    onClick: PropTypes.func.isRequired
  };

  render () {
    const { likes, onClick } = this.props
    return (
      <div className='likes-block'>
        <div className='likes-block--button'
             onClick={onClick}>
             МУР!
             <div className='likes-block--button-icon'><LikeButtonCat /></div>
             <div className='likes-block--count'>{likes}</div>
        </div>
      </div>
    )
  }
}

export default LikesBlock
