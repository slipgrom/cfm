import React, { PropTypes } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router'
import offset from 'document-offset'
import gsap from 'gsap'
import ScrollMagic from 'scrollmagic'
import 'imports?define=>false!scrollmagic/scrollmagic/uncompressed/plugins/animation.gsap'

import _ from 'lodash'
import { prettyNumber } from 'helpers/helpers'

import LikesBlock from './LikesBlock'
import CollectionAbout from './CollectionAbout'
import OtherItems from './OtherItems'
import SaleBlock from './SaleBlock'
import InstagramBlock from 'components/InstagramBlock/InstagramBlock'
import ShowMoreCollections from 'components/Common/ShowMoreCollections'
import ItemImage from 'components/Collection/ItemImage'
import WallpapersList from 'components/Collection/WallpapersList'
import WallpapersWidget from 'components/Collection/WallpapersWidget'
// import Panel from 'components/Collection/Panel'
import SizeContainer from './SizeContainer'

import StyleSwitcher from 'components/Common/StyleSwitcher'
import CollectionBadge from 'components/Common/CollectionBadge'
import LikeCounter from 'components/Common/LikeCounter'
import PanelInfo from './PanelInfo'
import { RubleSign, Mouse, BasketIcon } from 'components/Common/IconsCollection'

import { incrementLike } from 'redux/modules/collections'
import { addItem } from 'redux/modules/basket'
import {
  TSHIRT,
  WALLPAPER,
  POSTER,
  VINTAGE,
  MODERN
} from 'redux/modules/collections'

export class CollectionView extends React.Component {
  static propTypes = {
    collection: PropTypes.object.isRequired,
    params: PropTypes.object.isRequired,
    addItem: PropTypes.func.isRequired,
    incrementLike: PropTypes.func.isRequired,
    i18n: PropTypes.object.isRequired
  };

  constructor (...args) {
    super(...args)

    this.state = {
      style: VINTAGE,
      sex: 0,
      size: 0,
      height: 0,
      animated: false,
      pinned: false,
      openedPanel: false,
      rulesOpened: false
    }

    this.addBasketItem = this.addBasketItem.bind(this)
    this.renderContent = this.renderContent.bind(this)
    this.renderBasketButton = this.renderBasketButton.bind(this)
    this.setHeight = this.setHeight.bind(this)
    this.handleScroll = this.handleScroll.bind(this)
    this.handleResize = this.handleResize.bind(this)
    this.handleLikeClick = this.handleLikeClick.bind(this)
    this.handleRulesClick = this.handleRulesClick.bind(this)
  }

  componentDidMount () {
    this.setHeight()
    const headerHeight = Math.max(this.state.height, 865)
    const headerOffset = Math.max(headerHeight - window.innerHeight, 0)

    this.refs.collectionHeader.style.height = `${headerHeight}px`

    window.addEventListener('wheel', this.handleScroll)
    window.addEventListener('resize', this.handleResize)

    this.controller = new ScrollMagic.Controller({
      globalSceneOptions: {
        triggerHook: 'onLeave'
      }
    })

    const tween = gsap.fromTo(this.refs.collectionHeader, 0.5,
      {opacity: 1},
      {opacity: 0.1}
    )

    new ScrollMagic.Scene({
      triggerElement: this.refs.collectionHeader,
      offset: headerOffset
    })
    .setTween(tween)
    .setPin(this.refs.collectionHeader)
    .addTo(this.controller)

    new ScrollMagic.Scene({
      triggerElement: this.refs.collectionGallery,
      offset: headerOffset
    })
    .setPin(this.refs.collectionGallery)
    .addTo(this.controller)
    .on('start', () => {
      this.setState({
        openedPanel: false
      })
    })

    new ScrollMagic.Scene({
      triggerElement: this.refs.collectionContent
    })
    .addTo(this.controller)
    .on('enter', () => {
      this.setState({
        openedPanel: true
      })
    })
  }

  componentWillUnmount () {
    window.removeEventListener('wheel', this.handleScroll)
    window.removeEventListener('resize', this.handleResize)
  }

  render () {
    const { collection, params: { type, style } } = this.props
    const item = _.find(collection.items, {type: type})

    return (
      <div>
        <div ref='containerWrap' className='collection-view--container'>
          {this.renderPanel(item)}
          <div className='collection-view' ref='collectionHeader'>
            {this.renderCollectionHeader()}
          </div>
          <div className='collection-photos' ref='collectionGallery'>
            {this.renderSecondPage()}
          </div>
          <div ref='collectionContent' className='collection-view--content-container'>
            {(type === WALLPAPER) && <WallpapersList collectionId={collection.id} style={style} />}
            {(type === WALLPAPER) && item.content.map(this.renderWallpaperContent)}
            {(type !== WALLPAPER) && item.content.map(this.renderContent)}
            {(type === WALLPAPER) && <WallpapersWidget />}
            <CollectionAbout collection={collection} />
            <LikesBlock likes={collection.likes} onClick={this.handleLikeClick} />
          </div>
        </div>

        <SaleBlock />
        <OtherItems collection={collection} currentType={type} style={style} />
        <ShowMoreCollections />
      </div>
    )
  }

  renderSecondPage () {
    const { collection, params: { type } } = this.props
    const item = _.find(collection.items, {type: type})
    let galleryItems = []

    if (type === TSHIRT) {
      galleryItems = _.filter(item.gallery, {sex: this.state.sex, style: this.state.style})
    }
    if (type === WALLPAPER) {
      galleryItems = item.gallery
    }
    if (type === POSTER) {
      galleryItems = _.filter(item.gallery, {style: this.state.sex})
    }

    return (
      <div>
        {galleryItems.map((item, k) => {
          return (
            <div key={k} className='galleryItem'>
              <img src={item.src} />
            </div>
          )
        })}
        <div className='gallery-dots'>
          {galleryItems.map((item, k) => {
            const classNames = ['gallery-dot']
            if (k === 0) {
              classNames.push('active')
            }
            return (<span key={k} className={classNames.join(' ')}></span>)
          })}
        </div>
      </div>
    )
  }

  renderBasketButton () {
    const { params: { type } } = this.props
    const isWallpaper = type === WALLPAPER
    const text = isWallpaper ? 'Скачать в приложении' : 'В корзину'

    return (
      <a href='#' className='button' onClick={this.addBasketItem}>
        <BasketIcon />
        {text}
      </a>
    )
  }

  renderPanel (item) {
    const { collection, params: { type }, i18n } = this.props
    const { sex, size, style } = this.state
    const isTshirt = type === TSHIRT
    const isWallpaper = type === WALLPAPER

    let classes = ['panel']
    if (this.state.openedPanel) {
      classes.push('opened')
    }
    if (this.state.pinned) {
      classes.push('pinned')
    }

    return (
      <div className={classes.join(' ')}
           ref='panel'>
        <div className='panel-preview'>
          <ItemImage collectionId={collection.id} type={type} style={style} />
        </div>
        <div className='right-block'>
          {item.price && <span className='price'>{prettyNumber(item.price)} <RubleSign /></span>}
          {this.renderBasketButton()}
        </div>

        <div className='switchers'>
          <StyleSwitcher style={'light'}
                         activeElementIndex={style === MODERN ? 0 : 1}>
            <a href='#' onClick={(e) => {
              e.preventDefault()
              this.setState({style: MODERN})
            }}>Модерн</a>
            <a href='#' onClick={(e) => {
              e.preventDefault()
              this.setState({style: VINTAGE})
            }}>Винтаж</a>
          </StyleSwitcher>

          {isWallpaper && <StyleSwitcher style={'light'}
                                             activeElementIndex={0}
                                             elWidth={70}>
                                <a href='#'>iPhone</a>
                                <a href='#'>Android</a>
                              </StyleSwitcher>}

          {isTshirt && <StyleSwitcher style={'light'}
                                             activeElementIndex={sex}>
                                <a href='#' onClick={(e) => {
                                  e.preventDefault()
                                  this.setState({sex: 0})
                                }}>Ж</a>
                                <a href='#' onClick={(e) => {
                                  e.preventDefault()
                                  this.setState({sex: 1})
                                }}>М</a>
                              </StyleSwitcher>}

          {isTshirt && <SizeContainer size={size} onChange={(val) => { this.setState({size: val}) }} />}
        </div>
        {isTshirt && <PanelInfo i18n={i18n} />}
      </div>
    )
  }

  renderCollectionHeader () {
    const { collection, params: { type, style } } = this.props

    return (
      <div className='collection-header'>
        <img className='cat' src={collection.catSrc[style]} />
        <h1>{collection.title}</h1>
        <CollectionBadge type='hot' />
        <LikeCounter count={collection.likes} />
        <div className='carousel'>
          {this.renderItems(type, collection)}
        </div>
        <div className='mouse'>
          <Mouse />
        </div>
        <div className='mobileArrows'></div>
      </div>
    )
  }

  typesOrderList (type) {
    const order = [TSHIRT, POSTER, WALLPAPER]
    const index = _.indexOf(order, type)

    let newOrder = _.times(3, (k) => {
      return order[(index + k) % 3]
    })

    return [newOrder[2], newOrder[0], newOrder[1]]
  }

  renderItems (type, collection) {
    const { i18n } = this.props
    const { style } = this.state
    const order = this.typesOrderList(type)

    return order.map((t, k) => {
      const className = ['carousel-item']
      const href = `/ru/vintage/collections/${collection.id}/${_.toLower(t)}`
      className.push(`carousel-item-${k}`)
      className.push(`carousel-item-${_.toLower(t)}`)

      if (type === t) {
        className.push(`carousel-item-current`)
      }

      return (
        <Link to={href} key={k} className={className.join(' ')}>
          <span>{i18n.collection[_.toLower(t)]}</span>
          <ItemImage collectionId={collection.id} type={t} style={style} />
        </Link>
      )
    })
  }

  renderWallpaperContent (pageBlock, index) {
    if (index === 0) {
      return (
        <div key={index} className='content content-wallpaper-1'>
          <div dangerouslySetInnerHTML={{__html: pageBlock.html}}></div>
          <img src={pageBlock.img} />
        </div>
      )
    }
    if (index === 1) {
      return (
        <div key={index} className='content content-wallpaper-2'>
          <div dangerouslySetInnerHTML={{__html: pageBlock.html}}></div>
          <img src={pageBlock.img} />
        </div>
      )
    }
  }

  renderContent (pageBlock, index) {
    const { collection, params: { type } } = this.props
    const { style } = this.state

    if (index === 0) {
      return (
        <div key={index} className='content content-1'>
          <div dangerouslySetInnerHTML={{__html: pageBlock.html}}></div>
          <img className='imageLeft' src={pageBlock.img} />
          <img className='imageRight' src={pageBlock.img} />
        </div>
      )
    }

    if (index === 1) {
      return (
        <div key={index} className='content content-2'>
          <div dangerouslySetInnerHTML={{__html: pageBlock.html}}></div>
          <ItemImage collectionId={collection.id} type={type} style={style} />
        </div>
      )
    }

    if (pageBlock.type === 'instagram') {
      return (
        <InstagramBlock key={index} tag={pageBlock.tag} photos={pageBlock.photos} />
      )
    }

    if (index === 3) {
      const bgStyle = {
        backgroundImage: `url(/images/collection1/content/tshirt-3-bg.jpg)`
      }

      return (
        <div key={index} className={`content content-3 ${this.state.rulesOpened ? 'content-opened': ''}`} style={bgStyle}>
          <h3>{pageBlock.title}</h3>
          <h3>{pageBlock.title2}</h3>
          {pageBlock.icons && pageBlock.icons.map((icon, k) => (<div key={k} className='icons'><div className={`icon icon-${k}`}></div><span>{icon}</span></div>))}
          {this.state.rulesOpened && <div dangerouslySetInnerHTML={{__html: pageBlock.html}}></div>}
          <div className='rules' onClick={this.handleRulesClick}><span>{this.state.rulesOpened ? 'Свернуть' : pageBlock.rules}</span></div>
        </div>
      )
    }
  }

  addBasketItem () {
    const { size, style, sex } = this.state
    const { params: { type }, collection } = this.props
    const item = _.find(collection.items, {type: type})
    const types = {
      POSTER: 'постер',
      TSHIRT: 'футболка',
      WALLPAPER: 'обои'
    }

    const itemToBasket = {
      size: size,
      count: 1,
      style: style,
      sex: sex,
      id: item.id,
      collectionId: collection.id,
      collectionTitle: this.props.collection.title,
      type: type,
      typeTitle: types[type],
      price: item.price,
      url: item.src
    }

    this.props.addItem(itemToBasket)
  }

  handleScroll (e) {
    // pin panel before footer
    if (window.scrollY + window.innerHeight > offset(this.refs.containerWrap).top + this.refs.containerWrap.offsetHeight) {
      this.setState({
        pinned: true
      })
    } else {
      this.setState({
        pinned: false
      })
    }
  }

  handleResize (e) {
    this.setHeight()
  }

  setHeight () {
    const screenWidth = (window.innerWidth > 0) ? window.innerWidth : screen.width
    const offset = screenWidth >= 680 ? (80) : 0

    this.setState({
      height: (window.innerHeight - offset)
    })
  }

  handleLikeClick (e) {
    e.preventDefault()
    this.props.incrementLike(this.props.collection.id)
    return false
  }

  handleRulesClick () {
    this.setState({
      rulesOpened: !this.state.rulesOpened
    })
  }
}

function mapStateToProps (state, ownProps) {
  const { params } = ownProps
  const locale = state.locales.langs[state.locales.selectedLocale]
  const i18n = state.i18n[locale.title]

  return {
    collection: _.find(state.collections, { id: _.toInteger(params.id) }),
    i18n: i18n
  }
}

export default connect(mapStateToProps, {
  addItem,
  incrementLike
})(CollectionView)
