import React, { Component, PropTypes } from 'react'
import StyleSwitcher from 'components/Common/StyleSwitcher'

const sizes = {
  0: 'S',
  1: 'M',
  2: 'L',
  3: 'XL',
  4: 'XXL'
}

export class SizeContainer extends Component {
  static propTypes = {
    size: PropTypes.number.isRequired,
    onChange: PropTypes.func.isRequired
  };

  constructor (...args) {
    super(args)

    this.state = {
      hovered: false,
      hoveredOffset: 0
    }

    this.renderSize = this.renderSize.bind(this)
    this.handleMouseEnter = this.handleMouseEnter.bind(this)
    this.handleMouseLeave = this.handleMouseLeave.bind(this)
  }

  render () {
    const { size } = this.props
    const { hovered } = this.state

    return (
      <div className='panel__sizes_container'>
        <StyleSwitcher style={'light'}
                       activeElementIndex={size}>
          {Object.entries(sizes).map(this.renderSize)}
        </StyleSwitcher>
        {hovered && this.renderPopup()}
      </div>
    )
  }

  renderSize (entry) {
    const { onChange } = this.props

    return (
      <a key={entry[0]} href='#'
         onClick={(e) => {
           e.preventDefault()
           onChange(entry[0])
         }}
         onMouseEnter={this.handleMouseEnter}
         onMouseLeave={this.handleMouseLeave}>{entry[1]}</a>
    )
  }

  renderPopup () {
    const { hoveredOffset } = this.state
    const hoverStyle = {
      left: `${hoveredOffset}px`
    }

    return (
      <div style={hoverStyle} className='panel__popup_container'>
        <div className='panel__popup'>Осталось 6 футболок этого размера</div>
      </div>
    )
  }

  handleMouseEnter ({ target }) {
    const offset = target.offsetLeft + target.offsetWidth/2 + 3
    this.setState({
      hovered: true,
      hoveredOffset: offset
    })
  }

  handleMouseLeave (e) {
    this.setState({
      hovered: false,
      hoveredOffset: 0
    })
  }
}

export default SizeContainer
