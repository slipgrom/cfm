import React, { PropTypes } from 'react'
import { connect } from 'react-redux'

import { VideoIcon } from 'components/Common/IconsCollection'

const mapStateToProps = (state, ownProps) => {
  const author = state.authors[ownProps.collection.authorId]
  const locale = state.locales.langs[state.locales.selectedLocale]
  const i18n = state.i18n[locale.title]

  return {
    author: author,
    i18n: i18n
  }
}

export class CollectionAbout extends React.Component {
  static propTypes = {
    collection: PropTypes.object.isRequired,
    author: PropTypes.object.isRequired,
    i18n: PropTypes.object.isRequired
  };

  render () {
    const { author, collection, i18n } = this.props
    return (
      <div className='collection-author--about-container'>
        <div className='left-block'>
          <div className='collection-author'>
            <img className='collection-author--image' src={author.imageSrc} />
            <h3>{author.name}</h3>
            <h4>{i18n.collection.illustrator}</h4>
          </div>

          {collection.about && <div className='collection-author--about-collection'>
            {collection.about.map((p, k) => (<p key={k}>{p}</p>))}
          </div>}
        </div>
        <div className='right-block'>
          <h4>{i18n.collection.about}</h4>
          <div className='collection-author--about-author'>
            {author.about}
          </div>
          {author.links.length && <div className='links'>
                      {i18n.collection.links}:
                      {author.links.map((p, k) => (<p key={k}><a href={p}>{p}</a></p>))}
                    </div>}

          <div className='video-link'>
            <a href='#'><VideoIcon />{i18n.collection.videoLink}</a>
          </div>
        </div>
      </div>
    )
  }
}

export default connect(mapStateToProps)(CollectionAbout)
