import React, { PropTypes } from 'react'
import { connect } from 'react-redux'

import Select from 'react-select'

import { createFeedback } from 'redux/modules/feedback'

const mapStateToProps = (state) => ({
  feedback: state.feedback
})

export class FeedbackView extends React.Component {
  static propTypes = {
    feedback: PropTypes.object.isRequired,
    createFeedback: PropTypes.func.isRequired
  };

  constructor (...args) {
    super(...args)

    this.handleSendClick = this.handleSendClick.bind(this)
  }

  render () {
    return (
      <div>
        <div className='feedback-header'>
          <div className='title'>Обратная связь</div>
        </div>

        <div className='feedback-content'>
          <div className='feedback-form'>
            <form action='/' className='form'>
              <div className='feedback-form--left-block'>
                <div className='form-item'>
                  <label>Имя</label>
                  <div className='text-input'>
                    <input ref='name' type='text' name='feedback[name]' />
                  </div>
                </div>
                <div className='form-item'>
                  <label>Телефон</label>
                  <div className='text-input'>
                    <input ref='phone' type='text' name='feedback[phone]' />
                  </div>
                </div>
                <div className='form-item'>
                  <label>Электронная почта</label>
                  <div className='text-input'>
                    <input ref='email' type='text' name='feedback[email]' />
                  </div>
                </div>
              </div>

              <div className='feedback-form--right-block'>
                <div className='form-item'>
                  <label>Тема</label>
                  <div>
                    <Select name='feedback[subject]'
                            placeholder=''
                            options={[{value: 1, label: 'Сотрудничество'}, {value: 2, label: 'Сотрудничество 2'}]}/>
                  </div>
                </div>
                <div className='form-item'>
                  <label>Сообщение</label>
                  <div className='text-input'>
                    <textarea ref='message' name='feedback[message]'></textarea>
                  </div>
                </div>
              </div>
              <div className='send-button'>
                <button className='button' onClick={this.handleSendClick}>Отправить</button>
              </div>
            </form>
          </div>

          <div className='feedback-contacts'>
            <h3>Контакты для связи</h3>

            <div className='feedback-contacts--item'>
              <h4 className='label-title'>Социальные сети</h4>
            </div>

            <div className='feedback-contacts--item'>
              <h4 className='label-title'>Телефон</h4>
              <div className='phone'>+7 (495) 345-34-45</div>
            </div>

            <div className='feedback-contacts--item feedback-contacts--item-email'>
              <h4 className='label-title'>решение проблем с доставкой</h4>
              <div><a href='mailto:problems@catsfollow.me'>problems@catsfollow.me</a></div>
            </div>

            <div className='feedback-contacts--item feedback-contacts--item-email'>
              <h4 className='label-title'>для связи со сми</h4>
              <div><a href='mailto:smi@catsfollow.me'>smi@catsfollow.me</a></div>
            </div>

            <div className='feedback-contacts--item feedback-contacts--item-email'>
              <h4 className='label-title'>для общих вопросов</h4>
              <div><a href='mailto:inspiration@catsfollow.me'>inspiration@catsfollow.me</a></div>
            </div>
          </div>
        </div>
      </div>
    )
  }

  handleSendClick (e) {
    e.preventDefault()
    let data = {}
    let errors = []

    Object.keys(this.refs).map((k) => {
      data[k] = this.refs[k].value

      if (data[k] === '') {
        errors.push({[k]: 'Empty'})
      }
    })

    if (!errors.length) {
      this.props.createFeedback(data)
    } else {
      console.log('show errors', errors)
    }
  }
}

export default connect(mapStateToProps, {
  createFeedback
})(FeedbackView)
