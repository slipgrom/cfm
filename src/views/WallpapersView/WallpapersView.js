import React, { PropTypes } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router'
import _ from 'lodash'

import {
  WALLPAPER
} from 'redux/modules/collections'

import LikeCounter from 'components/Common/LikeCounter'
import ItemBanners from 'components/Common/ItemBanners'
import CollectionBadge from 'components/Common/CollectionBadge'
import PageNav from 'components/PageNav/PageNav'
import CollectionItem from 'components/Collection/CollectionItem'
import AnnouncementItem from 'components/Announcement/AnnouncementItem'

const mapStateToProps = (state) => ({
  collections: state.collections
})

export class WallpapersView extends React.Component {
  static propTypes = {
    collections: PropTypes.array.isRequired,
    params: PropTypes.object.isRequired
  };

  constructor (...args) {
    super(...args)
    this.renderItem = this.renderItem.bind(this)
  }

  render () {
    const { collections, params: { lang, style } } = this.props

    return (
      <div>
        {this.renderThirtsHeader()}
        <div className='listing'>
          {collections.map(this.renderItem)}
          <AnnouncementItem type={WALLPAPER} />
        </div>
        <PageNav />
        <ItemBanners currentType={WALLPAPER} lang={lang} style={style} />
      </div>
    )
  }

  renderThirtsHeader () {
    return (
      <div className='content-header content-header--wallpapers'>
        <div className='title'>Wallpapers</div>
      </div>
    )
  }

  renderItem (collection, index) {
    const { params: { lang, style } } = this.props
    const { items } = collection
    const item = _.find(items, {type: WALLPAPER})
    const itemStyle = {
      background: `url(/images/collection${collection.id}/bg.jpg) no-repeat center center`
    }
    const itemLink = `/${lang}/${style}/collections/${collection.id}/${item.type}`

    return (
      <div key={index} className='listing-item' style={itemStyle}>
        <CollectionBadge type='hot' />
        <LikeCounter count={collection.likes} />
        <Link to={itemLink}>
          <CollectionItem item={item} collectionId={collection.id} style={style} />
        </Link>
      </div>
    )
  }
}

export default connect(mapStateToProps)(WallpapersView)
