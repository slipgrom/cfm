import React, { PropTypes } from 'react'
import { connect } from 'react-redux'

import { Link } from 'react-router'

import _ from 'lodash'

import StyleSwitcher from 'components/Common/StyleSwitcher'
import CountChanger from 'components/Common/CountChanger'
import { RubleSign } from 'components/Common/IconsCollection'
import ItemImage from 'components/Collection/ItemImage'

import { changeSize, changeCount, removeItem, changeStyle } from 'redux/modules/basket'
import {
  TSHIRT
} from 'redux/modules/collections'

export class BasketView extends React.Component {
  static propTypes = {
    i18n: PropTypes.object.isRequired,
    basket: PropTypes.object.isRequired,
    changeCount: PropTypes.func.isRequired,
    removeItem: PropTypes.func.isRequired,
    changeSize: PropTypes.func.isRequired,
    changeStyle: PropTypes.func.isRequired
  };

  constructor (...args) {
    super(...args)

    this.renderItem = this.renderItem.bind(this)
    this.changeCountHandler = this.changeCountHandler.bind(this)
    this.clickStyleHandler = this.clickStyleHandler.bind(this)
    this.clickSexHandler = this.clickSexHandler.bind(this)
    this.renderTotal = this.renderTotal.bind(this)
    this.renderSaleTotal = this.renderSaleTotal.bind(this)
    this.clickSizeHandler = this.clickSizeHandler.bind(this)
  }

  render () {
    const { basket: { items } } = this.props

    return (
      <div className='basket-view'>
        <div className='content-header content-header--basket'>
          <div className='title'>Корзина</div>
        </div>
        {items.length ? items.map(this.renderItem) : this.renderEmptyBasket() }
        <div className='total-block'>
          <Link to='/ru/vintage/basket/delivery' className='rounded-button'>Оформить заказ</Link>
          <h3>Стоимость товаров {this.renderTotal()}</h3>
          <h4>Итого с учетом скидки 25% {this.renderSaleTotal()}</h4>
        </div>
      </div>
    )
  }

  renderItem (item, k) {
    const { i18n, basket: { sizes } } = this.props
    const isTshirt = item.type === TSHIRT
    const styleType = item.style === 0 ? 'modern' : 'vintage'

    return (
      <div key={k} className='basket-item'>
        <h3>{_.capitalize(i18n.collection[_.toLower(item.type)])} из коллекции &laquo;<a href='#'>{item.collectionTitle}</a>&raquo;</h3>
        <div className='image'><ItemImage collectionId={item.collectionId} type={item.type} style={styleType} /></div>
        <span className='price'>{item.price * item.count} Р</span>
        <span className='remove-link'
              onClick={this.handleRemoveClick.bind(this, k)}>Удалить</span>

        <div className='switchers'>
          <CountChanger onChangeCount={(c) => this.changeCountHandler(k, c)}
                        count={item.count} />

          <StyleSwitcher style={'grey'}
                         activeElementIndex={item.style}
                         elWidth={80}>
            <a href='#' onClick={(e) => this.clickStyleHandler(k, 0, e)}>Модерн</a>
            <a href='#' onClick={(e) => this.clickStyleHandler(k, 1, e)}>Винтаж</a>
          </StyleSwitcher>

          {isTshirt && <StyleSwitcher style={'grey'}
                                   activeElementIndex={item.sex}
                                   elWidth={40}>
                      <a href='#' onClick={() => this.clickSexHandler(k, 0)}>Ж</a>
                      <a href='#' onClick={() => this.clickSexHandler(k, 1)}>М</a>
                    </StyleSwitcher>}

          {isTshirt && <StyleSwitcher style={'grey'}
                                   activeElementIndex={item.size}
                                   elWidth={40}>
                      {Object.keys(sizes).map((index) =>
                        (<a key={index}
                            onClick={() => this.clickSizeHandler(k, index)}
                            href='#'>{sizes[index]}</a>)
                      )}
                    </StyleSwitcher>}
        </div>
      </div>
    )
  }

  renderEmptyBasket () {
    return (
      <div>Корзина пуста</div>
    )
  }

  handleRemoveClick (index, e) {
    e.stopPropagation()
    this.props.removeItem(index)
  }

  changeCountHandler (index, count) {
    this.props.changeCount(index, count)
  }

  clickSexHandler (index, sex) {

  }

  clickStyleHandler (index, style, e) {
    this.props.changeStyle(index, style)
  }

  clickSizeHandler (k, v) {
    const { basket: { sizes } } = this.props
    this.props.changeSize(k, sizes[v])
  }

  get total () {
    const { basket: { items } } = this.props
    let total = 0

    items.map((item) => {
      total += item.price * item.count
    })

    return total
  }

  renderTotal () {
    return (<span className='total'>{this.total} <RubleSign /></span>)
  }

  renderSaleTotal () {
    return (<span className='total'>{this.total - this.total * 0.25} <RubleSign /></span>)
  }
}

function mapStateToProps (state) {
  const locale = state.locales.langs[state.locales.selectedLocale]
  const i18n = state.i18n[locale.title]

  return {
    i18n: i18n,
    basket: state.basket
  }
}

export default connect(mapStateToProps, {
  changeCount,
  removeItem,
  changeSize,
  changeStyle
})(BasketView)
