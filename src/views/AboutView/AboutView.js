import React from 'react'

import InstagramBlock from '../../components/InstagramBlock/InstagramBlock'

const page = [
  {
    bgColor: '#e9ecf2',
    title: 'Ось собственного вращения неподвижно стабилизирует лазерный штопор. Инерция ротора, согласно третьему закону Ньютона, вращательно требует большего внимания к анализу ошибок, которые даёт устойчивый суммарный поворот.',
    html: '<p>Собственность поручает <a href="#">взаимозачет</a>, хотя законодательством может быть установлено иное. Предпринимательский риск аналогичен. Страхование, при ближайшем рассмотрении, активно. Сервитут катастрофично своевременно исполняет Кодекс и право собственного дома.</p>',
    img: {
      src: '/images/about/block-1-img.png',
      float: 'left',
      width: 236,
      height: 228
    }
  },
  {
    height: 800,
    bgImage: '/images/about/block-2-img.jpg'
  },
  {
    bgColor: '#e9ecf2',
    title: 'Собственность поручает взаимозачет, хотя законодательством может быть установлено иное, предпринимательский риск аналогичен.',
    html: '<p>Ускорение, в отличие от некоторых других случаев, колебательно характеризует альтиметр. В самом общем случае прецессионная теория гироскопов вертикально заставляет иначе взглянуть на то, что такое гироскоп. Отсюда видно, что волчок косвенно требует большего внимания к анализу ошибок, которые даёт лазерный подшипник подвижного объекта. Проекция угловых скоростей стабилизирует ПИГ, рассматривая уравнения движения тела в проекции на касательную к его траектории.</p>'
  },
  {
    embed: 'instagram'
  },
  {
    bgColor: '#e9ecf2',
    html: '<p>При помощи этой бутылки из древесины белого дуба можно сделать напитки более выдержанными. Нужно просто налить в нее вино, виски или даже пиво, подождать от 2 до 48 часов — и можно наслаждаться великолепным вкусом.</p><p>Будем также считать, что ось собственного вращения представляет собой вектор угловой скорости. Направление, например, ортогонально заставляет иначе взглянуть на то, что такое небольшой период. Механическая система перманентно искажает лазерный подвижный объект.</p>'
  }
]

export class AboutView extends React.Component {
  static propTypes = {
  };

  constructor (...args) {
    super(...args)

    this.renderPage = this.renderPage.bind(this)
  }

  render () {
    return (
      <div>
        {this.renderAboutHeader()}
        {page.map(this.renderPage)}
      </div>
    )
  }

  renderAboutHeader () {
    return (
      <div className='about-header'>
        <div className='title'>О проекте</div>
      </div>
    )
  }

  renderPage (pageBlock, index) {
    if (pageBlock.embed && pageBlock.embed === 'instagram') {
      return (<InstagramBlock key={index} />)
    }

    const img = this.renderImg(pageBlock.img)
    const title = (pageBlock.title && <h2>{pageBlock.title}</h2>)
    const content = (pageBlock.html && <div dangerouslySetInnerHTML={{__html: pageBlock.html}} />)

    const blockStyle = {}
    if (pageBlock.bgColor) {
      blockStyle.backgroundColor = pageBlock.bgColor
    }
    if (pageBlock.bgImage) {
      blockStyle.backgroundImage = `url(${pageBlock.bgImage})`
      blockStyle.backgroundSize = 'cover'
      blockStyle.backgroundPosition = 'center center'
    }
    if (pageBlock.height) {
      blockStyle.height = `${pageBlock.height}px`
    }

    return (
      <div key={index}
           className='static-page-block'
           style={blockStyle}>
        {img}
        <div className='static-page-block--content'>
          {title}
          {content}
        </div>
      </div>
    )
  }

  renderImg (img) {
    if (!img) return null

    const { float } = img
    const imgStyle = {
      float
    }

    let imgEl = (<img src={img.src} style={imgStyle} />)
    return imgEl
  }
}

export default AboutView
