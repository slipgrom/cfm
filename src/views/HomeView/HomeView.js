import React, { PropTypes } from 'react'
import { connect } from 'react-redux'

import IndexHeader from './IndexHeader'
import Collection from 'components/Collection/Collection'
import ShowMoreCollections from 'components/Common/ShowMoreCollections'
import Announcement from 'components/Announcement/Announcement'

const mapStateToProps = (state) => ({
  collections: state.collections
})

export class HomeView extends React.Component {
  static propTypes = {
    collections: PropTypes.array.isRequired,
    params: PropTypes.object.isRequired,
    i18n: PropTypes.object.isRequired
  };

  render () {
    const { collections, params: { style, lang } } = this.props

    return (
      <div>
        <IndexHeader {...this.props} />
        {collections && collections.map((collection, k) => {
          return <Collection key={k} collection={collection} style={style} lang={lang} />
        })}
        <Announcement />
        <ShowMoreCollections />
      </div>
    )
  }
}

export default connect(mapStateToProps)(HomeView)
