import React, { PropTypes } from 'react'
import { connect } from 'react-redux'

import IndexSlider from 'components/IndexSlider/IndexSlider'
import LargeButton from 'components/LargeButton/LargeButton'
import { Mouse } from 'components/Common/IconsCollection'

export class IndexHeader extends React.Component {
  static propTypes = {
    registration: PropTypes.object.isRequired,
    indexSlider: PropTypes.array.isRequired,
    i18n: PropTypes.object.isRequired
  };

  constructor (...args) {
    super(...args)
  }

  render () {
    const { i18n: { home } } = this.props
    return (
      <div className='index-header'>
        <div className='index-header-inner'>
          <IndexSlider items={this.props.indexSlider} />

          <img id='calligraphy' src='/images/calligraphy.png' />
          <div className='read-manifest'>
            <LargeButton text={home.readManifest} />
          </div>
          <div className='mouse'>
            <Mouse />
          </div>
        </div>
      </div>
    )
  }
}

function mapStateToProps (state) {
  return {
    registration: state.registration,
    indexSlider: state.indexSlider
  }
}

export default connect(mapStateToProps)(IndexHeader)
