import React, { PropTypes } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router'
import _ from 'lodash'

import {
  TSHIRT
} from 'redux/modules/collections'

import LikeCounter from 'components/Common/LikeCounter'
import ItemBanners from 'components/Common/ItemBanners'
import CollectionBadge from 'components/Common/CollectionBadge'
import PageNav from 'components/PageNav/PageNav'
import CollectionItem from 'components/Collection/CollectionItem'
import AnnouncementItem from 'components/Announcement/AnnouncementItem'
import StyleSwitcher from 'components/Common/StyleSwitcher'

const mapStateToProps = (state) => ({
  collections: state.collections
})

export class TshirtsView extends React.Component {
  static propTypes = {
    collections: PropTypes.array.isRequired,
    params: PropTypes.object.isRequired
  };

  constructor (...args) {
    super(...args)
    this.renderItem = this.renderItem.bind(this)
  }

  render () {
    const { collections, params: { lang, style } } = this.props
    const sex = 0

    return (
      <div>
        {this.renderThirtsHeader()}
        <div className='tshirt-view__sex-switcher'>
          <StyleSwitcher style={'dark'}
                         activeElementIndex={sex}>
            <a href='#' onClick={(e) => (this.setState({sex: 0}))}>Женские</a>
            <a href='#' onClick={(e) => (this.setState({sex: 1}))}>Мужские</a>
          </StyleSwitcher>
        </div>
        <div className='listing'>
          {collections.map(this.renderItem)}
          <AnnouncementItem type={TSHIRT} />
        </div>
        <PageNav />
        <ItemBanners currentType={TSHIRT} lang={lang} style={style} />
      </div>
    )
  }

  renderThirtsHeader () {
    return (
      <div className='tshirts-header'>
        <div className='title'>Футболки</div>
      </div>
    )
  }

  renderItem (collection, index) {
    const { params: { lang, style } } = this.props
    const { items } = collection
    const item = _.find(items, {type: TSHIRT})
    const itemStyle = {
      background: `url(/images/collection${collection.id}/bg.jpg) no-repeat center center`
    }
    const itemLink = `/${lang}/${style}/collections/${collection.id}/${item.type}`

    return (
      <div key={index} className='listing-item' style={itemStyle}>
        <CollectionBadge type='hot' />
        <LikeCounter count={collection.likes} />
        <Link to={itemLink}>
          <CollectionItem item={item} collectionId={collection.id} style={style} />
        </Link>
      </div>
    )
  }
}

export default connect(mapStateToProps)(TshirtsView)
