export default [{
  id: 1,
  title: 'Meditation',
  catSrc: {
    modern: '/images/collection1/modern/cat.png',
    vintage: '/images/collection1/vintage/cat.png'
  },
  likes: 23,
  authorId: 0,
  videoUrl: '',
  about: [
    'Собственность поручает взаимозачет, хотя законодательством может быть установлено иное. Предпринимательский риск аналогичен. Страхование, при ближайшем рассмотрении, активно.',
    'Ось ротора колебательно заставляет перейти к более сложной системе дифференциальных уравнений, если добавить вибрирующий подвес. Классическое уравнение движения связывает прецессирующий гирокомпас.'
  ],
  items: [
    {
      type: 'tshirt',
      src: '/images/collection1/tshirt.png',
      price: 2500,
      gallery: [
        {sex: 0, style: 'vintage', src: '/images/collection1/gallery1.jpg'},
        {sex: 1, style: 'vintage', src: '/images/collection1/gallery4.jpg'},
        {sex: 0, style: 'modern', src: '/images/collection1/gallery3.jpg'},
        {sex: 1, style: 'modern', src: '/images/collection1/gallery2.jpg'}
      ],
      content: [
        {
          html: `
            <h3>Стань холстом современного искусства</h3>
            <p>Футболка CatsFollowMe  —  это холст, на котором известный иллюстратор транслирует твое понимание искусства миру.</p>
            <h4>Made in Russia. With love.</h4>
          `,
          img: '/images/collection1/content/tshirt-1-bg.png'
        },
        {
          html: `
            <h3>Стиль, за которым следит мода</h3>
            <p>Футболка под пиджак, футболка с юбкой, футболка для клуба  —  какой выбор ты предпочитаешь сделать сегодня: «Винтаж» или «Модерн»?</p>
            <h5>Питайся вдохновением, созданным в коллекциях CatsFollowMe.</h5>
          `
        },
        {
          type: 'instagram',
          tag: 'catsfollowme',
          photos: [
            '/images/collection1/instagram/photo1.jpg',
            '/images/collection1/instagram/photo1.jpg',
            '/images/collection1/instagram/photo1.jpg',
            '/images/collection1/instagram/photo1.jpg',
            '/images/collection1/instagram/photo1.jpg'
          ]
        },
        {
          title: '100% Эко-хлопок.',
          title2: 'Плотный, легкий и практичный.',
          icons: [
            'Гладить теплым утюгом с внутренней стороны',
            'Не подвергать химической чистке',
            'Стирать при температуре 30°С',
            'Не сушить в барабане',
            'Не отбеливать'
          ],
          html: '<p>Собственность поручает взаимозачет, хотя законодательством может быть установлено иное. Предпринимательский риск аналогичен. Страхование, при ближайшем рассмотрении, активно.</p><p>В случае если возникли какие-то проблемы с изделием, мы поменяем футболку на другую, бесплатно!</p>',
          rules: 'Правила ухода'

        }
      ]
    }, {
      type: 'poster',
      src: {
        modern: '/images/collection1/modern/poster.jpg',
        vintage: '/images/collection1/vintage/poster.jpg'
      },
      price: 2500,
      gallery: [
        {style: 'vintage', src: '/images/collection1/gallery1.jpg'},
        {style: 'vintage', src: '/images/collection1/gallery4.jpg'},
        {style: 'modern', src: '/images/collection1/gallery3.jpg'},
        {style: 'modern', src: '/images/collection1/gallery2.jpg'}
      ],
      content: [
        {
          html: `
            <h3>Искусство, рожденное вдохновением</h3>
            <p>Постеры CatsFollowMe  —  современное искусство, создаваемое  лучшими иллюстраторами для моделирования твоей атмосферы.</p>
            <h4>Ракурс твоего стиля.</h4>
          `,
          img: '/images/collection1/content/tshirt-1-bg.png'
        },
        {
          html: `
            <h3>Создавай. Проявляй. Твори свою реальность.</h3>
            <p>Фактурный дизайнерский элемент украшения любого интерьера: дом, офис, кафе — преображай.</p>
            <h5>Уникальная атмосфера в любом пространстве.</h5>
          `
        },
        {
          type: 'instagram',
          tag: 'catsfollowme',
          photos: [
            '/images/collection1/instagram/photo1.jpg',
            '/images/collection1/instagram/photo1.jpg',
            '/images/collection1/instagram/photo1.jpg',
            '/images/collection1/instagram/photo1.jpg',
            '/images/collection1/instagram/photo1.jpg'
          ]
        },
        {
          title: 'Качественный постер из плотной бумаги формата: 50 Х 70 см.',
          // icons: [],
          rules: 'Рекомендации по использованию'
        }
      ]
    }, {
      type: 'wallpaper',
      src: {
        modern: '/images/collection1/modern/wallpaper.jpg',
        vintage: '/images/collection1/vintage/wallpaper.jpg'
      },
      price: null,
      gallery: [
        {style: 'vintage', src: '/images/collection1/gallery1.jpg'},
        {style: 'vintage', src: '/images/collection1/gallery4.jpg'},
        {style: 'modern', src: '/images/collection1/gallery3.jpg'},
        {style: 'modern', src: '/images/collection1/gallery2.jpg'}
      ],
      content: [
        {
          html: `
            <h3>Оригинально. Вдохновляй других.</h3>
            <p>Wallpaper от CatsFollowMe  —  это часть концепции бренда, достойно дополняющая твои коллекции футболок и постеров.</p>
            <p>Прикольные. Стильные. От известных иллюстраторов.</p>
          `,
          img: '/images/collection1/content/phone-1.png'
        },
        {
          html: `
            <h3>Обновляй. Дополняй. Наслаждайся.</h3>
            <p>Скачай удобное приложение для смартфона и получай новые коллекции wallpaper’s CatsFollowMe бесплатно.</p>
            <p>Просто быть стильным вместе с CatsFollowMe.</p>
          `,
          img: '/images/collection1/content/phone-2.png'
        },
        {
          type: 'instagram',
          tag: 'catsfollowme',
          photos: [
            '/images/collection1/instagram/photo1.jpg',
            '/images/collection1/instagram/photo1.jpg',
            '/images/collection1/instagram/photo1.jpg',
            '/images/collection1/instagram/photo1.jpg',
            '/images/collection1/instagram/photo1.jpg'
          ]
        },
        {
          title: '100% Эко-хлопок.',
          title2: 'Плотный, легкий и практичный.',
          icons: [],
          rules: 'Правила ухода'
        }
      ]
    }
  ]
},

{
  id: 2,
  title: 'Pixel times',
  catSrc: {
    modern: '/images/collection2/modern/cat.png',
    vintage: '/images/collection2/vintage/cat.png'
  },
  likes: 23,
  authorId: 0,
  videoUrl: '',
  about: [
    'Собственность поручает взаимозачет, хотя законодательством может быть установлено иное. Предпринимательский риск аналогичен. Страхование, при ближайшем рассмотрении, активно.',
    'Ось ротора колебательно заставляет перейти к более сложной системе дифференциальных уравнений, если добавить вибрирующий подвес. Классическое уравнение движения связывает прецессирующий гирокомпас.'
  ],
  items: [
    {
      type: 'tshirt',
      src: '/images/collection2/tshirt.png',
      price: 2500,
      gallery: [
        {sex: 0, style: 'vintage', src: '/images/collection1/gallery1.jpg'},
        {sex: 1, style: 'vintage', src: '/images/collection1/gallery4.jpg'},
        {sex: 0, style: 'modern', src: '/images/collection1/gallery3.jpg'},
        {sex: 1, style: 'modern', src: '/images/collection1/gallery2.jpg'}
      ],
      content: [
        {
          html: `
            <h3>Стань холстом современного искусства</h3>
            <p>Футболка CatsFollowMe  —  это холст, на котором известный иллюстратор транслирует твое понимание искусства миру.</p>
            <h4>Made in Russia. With love.</h4>
          `,
          img: '/images/collection1/content/tshirt-1-bg.png'
        },
        {
          html: `
            <h3>Стиль, за которым следит мода</h3>
            <p>Футболка под пиджак, футболка с юбкой, футболка для клуба  —  какой выбор ты предпочитаешь сделать сегодня: «Винтаж» или «Модерн»?</p>
            <h5>Питайся вдохновением, созданным в коллекциях CatsFollowMe.</h5>
          `
        },
        {
          type: 'instagram',
          tag: 'catsfollowme',
          photos: [
            '/images/collection1/instagram/photo1.jpg',
            '/images/collection1/instagram/photo1.jpg',
            '/images/collection1/instagram/photo1.jpg',
            '/images/collection1/instagram/photo1.jpg',
            '/images/collection1/instagram/photo1.jpg'
          ]
        },
        {
          title: '100% Эко-хлопок.',
          title2: 'Плотный, легкий и практичный.',
          icons: [
            'Гладить теплым утюгом с внутренней стороны',
            'Не подвергать химической чистке',
            'Стирать при температуре 30°С',
            'Не сушить в барабане',
            'Не отбеливать'
          ],
          html: '<p>Собственность поручает взаимозачет, хотя законодательством может быть установлено иное. Предпринимательский риск аналогичен. Страхование, при ближайшем рассмотрении, активно.</p><p>В случае если возникли какие-то проблемы с изделием, мы поменяем футболку на другую, бесплатно!</p>',
          rules: 'Правила ухода'

        }
      ]
    }, {
      type: 'poster',
      src: {
        modern: '/images/collection2/modern/poster.jpg',
        vintage: '/images/collection2/vintage/poster.jpg'
      },
      price: 2500,
      gallery: [
        {sex: 0, src: '/images/collection1/gallery1.jpg'},
        {sex: 0, src: '/images/collection1/gallery1.jpg'},
        {sex: 1, src: '/images/collection1/gallery1.jpg'}
      ],
      content: [
        {
          html: `
            <h3>Искусство, рожденное вдохновением</h3>
            <p>Постеры CatsFollowMe  —  современное искусство, создаваемое  лучшими иллюстраторами для моделирования твоей атмосферы.</p>
            <h4>Ракурс твоего стиля.</h4>
          `,
          img: '/images/collection1/content/tshirt-1-bg.png'
        },
        {
          html: `
            <h3>Создавай. Проявляй. Твори свою реальность.</h3>
            <p>Фактурный дизайнерский элемент украшения любого интерьера: дом, офис, кафе — преображай.</p>
            <h5>Уникальная атмосфера в любом пространстве.</h5>
          `
        },
        {
          type: 'instagram',
          tag: 'catsfollowme',
          photos: [
            '/images/collection1/instagram/photo1.jpg',
            '/images/collection1/instagram/photo1.jpg',
            '/images/collection1/instagram/photo1.jpg',
            '/images/collection1/instagram/photo1.jpg',
            '/images/collection1/instagram/photo1.jpg'
          ]
        },
        {
          title: 'Качественный постер из плотной бумаги формата: 50 Х 70 см.',
          // icons: [],
          rules: 'Рекомендации по использованию'
        }
      ]
    }, {
      type: 'wallpaper',
      src: {
        modern: '/images/collection1/modern/wallpaper.jpg',
        vintage: '/images/collection1/vintage/wallpaper.jpg'
      },
      price: null,
      gallery: [
        {src: '/images/collection1/wallpaperGallery/slide1.jpg'}
      ],
      content: [
        {
          html: `
            <h3>Оригинально. Вдохновляй других.</h3>
            <p>Wallpaper от CatsFollowMe  —  это часть концепции бренда, достойно дополняющая твои коллекции футболок и постеров.</p>
            <p>Прикольные. Стильные. От известных иллюстраторов.</p>
          `,
          img: '/images/collection1/content/phone-1.png'
        },
        {
          html: `
            <h3>Обновляй. Дополняй. Наслаждайся.</h3>
            <p>Скачай удобное приложение для смартфона и получай новые коллекции wallpaper’s CatsFollowMe бесплатно.</p>
            <p>Просто быть стильным вместе с CatsFollowMe.</p>
          `,
          img: '/images/collection1/content/phone-2.png'
        },
        {
          type: 'instagram',
          tag: 'catsfollowme',
          photos: [
            '/images/collection1/instagram/photo1.jpg',
            '/images/collection1/instagram/photo1.jpg',
            '/images/collection1/instagram/photo1.jpg',
            '/images/collection1/instagram/photo1.jpg',
            '/images/collection1/instagram/photo1.jpg'
          ]
        },
        {
          title: '100% Эко-хлопок.',
          title2: 'Плотный, легкий и практичный.',
          icons: [],
          rules: 'Правила ухода'
        }
      ]
    }
  ]
},

{
  id: 3,
  title: 'First Contact',
  catSrc: {
    modern: '/images/collection3/modern/cat.png',
    vintage: '/images/collection3/vintage/cat.png'
  },
  likes: 23,
  authorId: 0,
  videoUrl: '',
  about: [
    'Собственность поручает взаимозачет, хотя законодательством может быть установлено иное. Предпринимательский риск аналогичен. Страхование, при ближайшем рассмотрении, активно.',
    'Ось ротора колебательно заставляет перейти к более сложной системе дифференциальных уравнений, если добавить вибрирующий подвес. Классическое уравнение движения связывает прецессирующий гирокомпас.'
  ],
  items: [
    {
      type: 'tshirt',
      src: '/images/collection1/tshirt.png',
      price: 2500,
      gallery: [
        {sex: 0, style: 'vintage', src: '/images/collection1/gallery1.jpg'},
        {sex: 1, style: 'vintage', src: '/images/collection1/gallery4.jpg'},
        {sex: 0, style: 'modern', src: '/images/collection1/gallery3.jpg'},
        {sex: 1, style: 'modern', src: '/images/collection1/gallery2.jpg'}
      ],
      content: [
        {
          html: `
            <h3>Стань холстом современного искусства</h3>
            <p>Футболка CatsFollowMe  —  это холст, на котором известный иллюстратор транслирует твое понимание искусства миру.</p>
            <h4>Made in Russia. With love.</h4>
          `,
          img: '/images/collection1/content/tshirt-1-bg.png'
        },
        {
          html: `
            <h3>Стиль, за которым следит мода</h3>
            <p>Футболка под пиджак, футболка с юбкой, футболка для клуба  —  какой выбор ты предпочитаешь сделать сегодня: «Винтаж» или «Модерн»?</p>
            <h5>Питайся вдохновением, созданным в коллекциях CatsFollowMe.</h5>
          `
        },
        {
          type: 'instagram',
          tag: 'catsfollowme',
          photos: [
            '/images/collection1/instagram/photo1.jpg',
            '/images/collection1/instagram/photo1.jpg',
            '/images/collection1/instagram/photo1.jpg',
            '/images/collection1/instagram/photo1.jpg',
            '/images/collection1/instagram/photo1.jpg'
          ]
        },
        {
          title: '100% Эко-хлопок.',
          title2: 'Плотный, легкий и практичный.',
          icons: [
            'Гладить теплым утюгом с внутренней стороны',
            'Не подвергать химической чистке',
            'Стирать при температуре 30°С',
            'Не сушить в барабане',
            'Не отбеливать'
          ],
          html: '<p>Собственность поручает взаимозачет, хотя законодательством может быть установлено иное. Предпринимательский риск аналогичен. Страхование, при ближайшем рассмотрении, активно.</p><p>В случае если возникли какие-то проблемы с изделием, мы поменяем футболку на другую, бесплатно!</p>',
          rules: 'Правила ухода'

        }
      ]
    }, {
      type: 'poster',
      src: {
        modern: '/images/collection1/modern/poster.jpg',
        vintage: '/images/collection1/vintage/poster.jpg'
      },
      price: 2500,
      gallery: [
        {sex: 0, src: '/images/collection1/gallery1.jpg'},
        {sex: 0, src: '/images/collection1/gallery1.jpg'},
        {sex: 1, src: '/images/collection1/gallery1.jpg'}
      ],
      content: [
        {
          html: `
            <h3>Искусство, рожденное вдохновением</h3>
            <p>Постеры CatsFollowMe  —  современное искусство, создаваемое  лучшими иллюстраторами для моделирования твоей атмосферы.</p>
            <h4>Ракурс твоего стиля.</h4>
          `,
          img: '/images/collection1/content/tshirt-1-bg.png'
        },
        {
          html: `
            <h3>Создавай. Проявляй. Твори свою реальность.</h3>
            <p>Фактурный дизайнерский элемент украшения любого интерьера: дом, офис, кафе — преображай.</p>
            <h5>Уникальная атмосфера в любом пространстве.</h5>
          `
        },
        {
          type: 'instagram',
          tag: 'catsfollowme',
          photos: [
            '/images/collection1/instagram/photo1.jpg',
            '/images/collection1/instagram/photo1.jpg',
            '/images/collection1/instagram/photo1.jpg',
            '/images/collection1/instagram/photo1.jpg',
            '/images/collection1/instagram/photo1.jpg'
          ]
        },
        {
          title: 'Качественный постер из плотной бумаги формата: 50 Х 70 см.',
          // icons: [],
          rules: 'Рекомендации по использованию'
        }
      ]
    }, {
      type: 'wallpaper',
      src: {
        modern: '/images/collection1/modern/wallpaper.jpg',
        vintage: '/images/collection1/vintage/wallpaper.jpg'
      },
      price: null,
      gallery: [
        {src: '/images/collection1/wallpaperGallery/slide1.jpg'}
      ],
      content: [
        {
          html: `
            <h3>Оригинально. Вдохновляй других.</h3>
            <p>Wallpaper от CatsFollowMe  —  это часть концепции бренда, достойно дополняющая твои коллекции футболок и постеров.</p>
            <p>Прикольные. Стильные. От известных иллюстраторов.</p>
          `,
          img: '/images/collection1/content/phone-1.png'
        },
        {
          html: `
            <h3>Обновляй. Дополняй. Наслаждайся.</h3>
            <p>Скачай удобное приложение для смартфона и получай новые коллекции wallpaper’s CatsFollowMe бесплатно.</p>
            <p>Просто быть стильным вместе с CatsFollowMe.</p>
          `,
          img: '/images/collection1/content/phone-2.png'
        },
        {
          type: 'instagram',
          tag: 'catsfollowme',
          photos: [
            '/images/collection1/instagram/photo1.jpg',
            '/images/collection1/instagram/photo1.jpg',
            '/images/collection1/instagram/photo1.jpg',
            '/images/collection1/instagram/photo1.jpg',
            '/images/collection1/instagram/photo1.jpg'
          ]
        },
        {
          title: '100% Эко-хлопок.',
          title2: 'Плотный, легкий и практичный.',
          icons: [],
          rules: 'Правила ухода'
        }
      ]
    }
  ]
}]
