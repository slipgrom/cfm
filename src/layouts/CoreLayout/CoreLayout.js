import React, { PropTypes } from 'react'
import { connect } from 'react-redux'

import Registration from 'components/Header/Registration'
import InnerHeader from 'components/Header/InnerHeader'
import MainMenu from 'components/MainMenu/MainMenu'
import Footer from 'components/Footer/Footer'

import LoadingScreen from 'layouts/LoadingScreen/LoadingScreen'

import 'styles/core.scss'

import {
  setLoading
} from 'redux/modules/menus'

export class CoreLayout extends React.Component {
  static propTypes = {
    children: PropTypes.element,
    registration: PropTypes.object.isRequired,
    style: PropTypes.number.isRequired,
    routes: PropTypes.array,
    params: PropTypes.object,
    history: PropTypes.object,
    i18n: PropTypes.object.isRequired,
    loading: PropTypes.bool.isRequired,
    setLoading: PropTypes.func
  };

  static contextTypes = {
    router: React.PropTypes.object.isRequired
  };

  constructor (...args) {
    super(args)

    this.goToUrl = this.goToUrl.bind(this)
    this.changeSiteLang = this.changeSiteLang.bind(this)
    this.changeSiteStyle = this.changeSiteStyle.bind(this)
  }

  componentDidMount () {
    setTimeout(() => {
      this.props.setLoading(false)
    }, 2500)
  }

  render () {
    const { i18n, loading } = this.props
    const isHomeView = /HomeView/.test(this.props.children.type.displayName)
    const isCollectionView = /CollectionView/.test(this.props.children.type.displayName)

    if (loading) {
      return (<LoadingScreen />)
    }

    return (
      <div className={`style__${this.props.params.style}`}>
        <div className='layout'>
          <MainMenu onStyleChange={this.changeSiteStyle}
            onLangChange={this.changeSiteLang}
            activeLang={this.props.params.lang}
            activeStyle={this.props.params.style} {...this.props} />
          {this.props.registration.opened && <Registration />}
          <InnerHeader isHomeView={isHomeView} isCollectionView={isCollectionView} {...this.props}/>
          {React.cloneElement(this.props.children, { i18n: i18n })}
          <Footer {...this.props} />
        </div>
      </div>
    )
  }

  changeSiteStyle (style) {
    this.goToUrl({ style })
  }

  changeSiteLang (lang) {
    this.goToUrl({ lang })
  }

  goToUrl (newParam) {
    let url = ''
    this.props.routes.map((route, k) => {
      if (k !== 0 && route.path) {
        url += `/${route.path}`
      }
    })

    let newParams = Object.assign({}, this.props.params, newParam)

    Object.keys(newParams).map((key) => {
      url = url.replace(':' + key, newParams[key])
    })

    this.context.router.push(url)
  }
}

function mapStateToProps (state, ownProps) {
  const i18n = state.i18n[ownProps.params.lang]

  return {
    registration: state.registration,
    style: state.menus.siteStyle,
    i18n: i18n,
    loading: state.menus.loading
  }
}

export default connect(mapStateToProps, {
  setLoading
})(CoreLayout)
