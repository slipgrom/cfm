import React from 'react'
import bgImg from './loading-bg.png'
import catImg from './cat.png'
import TweenMax from 'gsap'

class LoadingScreen extends React.Component {
  componentDidMount () {
    TweenMax.fromTo(this.refs.cat, 2, {
      height: 0
    }, {
      height: 71,
      repeat: -1
    })
  }

  render () {
    const style = {
      backgroundImage: `url(${bgImg})`
    }

    return (
      <div className='loading-screen' style={style}>
        <div className='loading-screen--cat'>
          <div ref='cat' className='loading-screen--loader'>
            <img src={catImg} />
          </div>
        </div>
      </div>
    )
  }
}

export default LoadingScreen
