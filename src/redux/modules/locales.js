/* @flow */
// ------------------------------------
// Constants
// ------------------------------------
export const SET_LOCALE = 'SET_LOCALE'

// ------------------------------------
// Actions
// ------------------------------------
// NOTE: "Action" is a Flow interface defined in https://github.com/TechnologyAdvice/flow-interfaces
// If you're unfamiliar with Flow, you are completely welcome to avoid annotating your code, but
// if you'd like to learn more you can check out: flowtype.org.
export const setLocale = (localeId) => ({
  type: SET_LOCALE,
  payload: {
    selectedLocale: localeId
  }
})

// export const actions = {
//   increment
// }

// ------------------------------------
// Action Handlers
// ------------------------------------
const ACTION_HANDLERS = {
  SET_LOCALE: (state, action) => {
    return Object.assign({}, state, action.payload)
  }
}

// ------------------------------------
// Reducer
// ------------------------------------
const initialState = {
  selectedLocale: 0,
  langs: {
    0: { id: 0, title: 'ru' },
    1: { id: 1, title: 'en' }
  }
}

export default function localesReducer (state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type]

  return handler ? handler(state, action) : state
}
