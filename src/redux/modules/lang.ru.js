import {
  TSHIRT,
  WALLPAPER,
  POSTER
} from 'redux/modules/collections'

export default {
  modern: 'Модерн',
  vintage: 'Винтаж',
  topMenu: {
    'about': 'О проекте',
    'blog': 'Блог',
    'delivery': 'Доставка и оплата',
    'feedback': 'Обратная связь'
  },
  mainMenu: {
    'collections': 'Коллекции',
    'tshirts': 'Футболки',
    'wallpapers': 'Обои',
    'posters': 'Постеры'
  },
  basket: 'Корзина',
  cabinet: 'Кабинет',
  rights: 'Все права защищены',
  madeIn: 'Сделано в <a href="http://zendesign.ru/ru/">Дзен Дизайн</a>',
  home: {
    readManifest: 'Прочитать манифест'
  },
  collection: {
    illustrator: 'Иллюстратор коллекции',
    about: 'Об иллюстраторе',
    links: 'Ссылки',
    videoLink: 'Видео создания коллекции',
    sale: 'Скидка',
    wallpaper: 'Обои',
    tshirt: 'Футболка',
    poster: 'Постер',
    instagram: {
      title: 'Эта коллекция в инстаграмме',
      footerText: 'Cфотографируйся в нашей продукции, размести фото с хештегом <span>#${tag}</span> и мы разместим твою фотографию в этом разделе.'
    }
  },
  announcement: {
    largeInputTitle: 'Оповестить об ежемесячном обновлении коллекций',
    shortInputTitle: 'Узнать первым',
    placeholder: 'Введите email',
    [TSHIRT]: {
      title: 'До выхода новой футболки осталось'
    },
    [POSTER]: {
      title: 'До выхода нового постера осталось'
    },
    [WALLPAPER]: {
      title: 'До выхода новых обоев осталось'
    }
  },
  common: {
    infoTable: 'Таблица размеров'
  }
}
