// ------------------------------------
// Constants
// ------------------------------------

// ------------------------------------
// Actions
// ------------------------------------

// ------------------------------------
// Action Handlers
// ------------------------------------
const ACTION_HANDLERS = {
}

// ------------------------------------
// Reducer
// ------------------------------------
const initialState = [
  {
    iconUrl: '/images/indexSlider/icon-1.png',
    slideUrl: '/images/indexSlider/slide-1.png'
  },
  {
    iconUrl: '/images/indexSlider/icon-2.png',
    slideUrl: '/images/indexSlider/slide-2.png'
  },
  {
    iconUrl: '/images/indexSlider/icon-3.png',
    slideUrl: '/images/indexSlider/slide-3.png'
  }
]

export default function reducer (state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type]

  return handler ? handler(state, action) : state
}
