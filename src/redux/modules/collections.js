import _ from 'lodash'
import collectionsJson from '../../static/collectionsJson'

// ------------------------------------
// Constants
// ------------------------------------
export const VINTAGE = 'vintage'
export const MODERN = 'modern'

export const TSHIRT = 'tshirt'
export const WALLPAPER = 'wallpaper'
export const POSTER = 'poster'

export const SET_LOCALE = 'SET_LOCALE'
export const INCREMENT_LIKE = 'INCREMENT_LIKE'

// ------------------------------------
// Actions
// ------------------------------------
export const setLocale = (localeId) => ({
  type: SET_LOCALE,
  payload: {
    selectedLocale: localeId
  }
})
export const incrementLike = (id) => ({
  type: INCREMENT_LIKE,
  id
})

// export const actions = {
//   increment
// }

// ------------------------------------
// Action Handlers
// ------------------------------------
const ACTION_HANDLERS = {
  SET_LOCALE: (state, action) => {
    return Object.assign({}, state, action.payload)
  },
  INCREMENT_LIKE: (state, action) => {
    const collection = _.find(state, {id: action.id})
    const index = _.findIndex(state, collection)
    collection.likes = collection.likes + 1

    return [_.slice(state, 0, index), collection, _.slice(state, index + 1)]
  }
}

// ------------------------------------
// Reducer
// ------------------------------------
const initialState = collectionsJson

export default function collectionsReducer (state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type]

  return handler ? handler(state, action) : state
}
