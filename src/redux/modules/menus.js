/* @flow */
// ------------------------------------
// Constants
// ------------------------------------
export const SET_MAIN_MENU_ACTIVE_ITEM = 'SET_MAIN_MENU_ACTIVE_ITEM'
export const SET_TOP_MENU_ACTIVE_ITEM = 'SET_TOP_MENU_ACTIVE_ITEM'
export const SET_SITE_STYLE = 'SET_SITE_STYLE'
export const SET_LOADING = 'SET_LOADING'

// ------------------------------------
// Actions
// ------------------------------------
// NOTE: "Action" is a Flow interface defined in https://github.com/TechnologyAdvice/flow-interfaces
// If you're unfamiliar with Flow, you are completely welcome to avoid annotating your code, but
// if you'd like to learn more you can check out: flowtype.org.
export const setMainMenuActiveItem = (index) => ({
  type: SET_MAIN_MENU_ACTIVE_ITEM,
  payload: {
    mainMenu: {
      activeItemIndex: index
    }
  }
})
export const setTopMenuActiveItem = (index) => ({
  type: SET_TOP_MENU_ACTIVE_ITEM,
  payload: {
    mainMenu: {
      activeItemIndex: index
    }
  }
})
export const setSiteStyle = (siteStyle) => ({
  type: SET_SITE_STYLE,
  payload: {
    siteStyle
  }
})

export const setLoading = (loading) => ({
  type: SET_LOADING,
  payload: {
    loading
  }
})

// ------------------------------------
// Action Handlers
// ------------------------------------
const ACTION_HANDLERS = {
  SET_MAIN_MENU_ACTIVE_ITEM: (state, action) => state + action.payload,
  SET_TOP_MENU_ACTIVE_ITEM: (state, action) => state + action.payload,
  SET_SITE_STYLE: (state, action) => state + action.payload,
  SET_LOADING: (state, action) => Object.assign({}, state, action.payload)
}

// ------------------------------------
// Reducer
// ------------------------------------
const initialState = {
  mainMenu: {
    activeItemIndex: 0,
    items: [
      {url: 'collections'},
      {url: 'tshirts'},
      {url: 'wallpapers'},
      {url: 'posters'}
    ]
  },
  topMenu: {
    activeItemIndex: 0,
    items: [
      {url: 'about'},
      {url: 'blog'},
      {url: 'delivery'},
      {url: 'feedback'}
    ]
  },
  siteStyle: 1,
  loading: true
}

export default function menuReducer (state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type]

  return handler ? handler(state, action) : state
}
