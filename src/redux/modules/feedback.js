import { CALL_API } from 'redux-api-middleware'

/* @flow */
// ------------------------------------
// Constants
// ------------------------------------
export const CREATE_FEEDBACK = 'CREATE_FEEDBACK'
export const CREATE_FEEDBACK_OK = 'CREATE_FEEDBACK_OK'
export const CREATE_FEEDBACK_FAIL = 'CREATE_FEEDBACK_FAIL'

// ------------------------------------
// Actions
// ------------------------------------
// NOTE: "Action" is a Flow interface defined in https://github.com/TechnologyAdvice/flow-interfaces
// If you're unfamiliar with Flow, you are completely welcome to avoid annotating your code, but
// if you'd like to learn more you can check out: flowtype.org.

export const createFeedback = (data) => {
  console.log(data, CALL_API)
  return {
    [CALL_API]: {
      endpoint: '/test.json',
      method: 'GET',
      types: [CREATE_FEEDBACK, CREATE_FEEDBACK_OK, CREATE_FEEDBACK_FAIL]
    }
  }
}

// ------------------------------------
// Action Handlers
// ------------------------------------
const ACTION_HANDLERS = {
  SET_LOCALE: (state, action) => {
    return Object.assign({}, state, action.payload)
  },
  CREATE_FEEDBACK: (state, action) => {
    console.log('create', state, action)
    return state
  },
  CREATE_FEEDBACK_OK: (state, action) => {
    console.log('ok', state, action)
    return state
  }
}

// ------------------------------------
// Reducer
// ------------------------------------
const initialState = {
}

export default function feedbackReducer (state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type]

  return handler ? handler(state, action) : state
}
