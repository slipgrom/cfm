const ACTION_HANDLERS = {}

const initialState = {
  title: 'Каллиграфический геометрический мир',
  releaseDate: '2016 03 24',
  bgSrc: '/images/announcement/bg.jpg'
}

export default function announcementReducer (state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type]

  return handler ? handler(state, action) : state
}
