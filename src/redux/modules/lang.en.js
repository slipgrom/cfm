import {
  TSHIRT,
  WALLPAPER,
  POSTER
} from 'redux/modules/collections'

export default {
  modern: 'Modern',
  vintage: 'Vintage',
  topMenu: {
    'about': 'About',
    'blog': 'Blog',
    'delivery': 'Delivery & payment',
    'feedback': 'Feedback'
  },
  mainMenu: {
    'collections': 'Collections',
    'tshirts': 'Tshirts',
    'wallpapers': 'Wallpapers',
    'posters': 'Posters'
  },
  basket: 'Basket',
  cabinet: 'Cabinet',
  rights: 'All right reserved',
  madeIn: 'Made in <a href="http://zendesign.ru/en/">Zen Design</a>',
  home: {
    readManifest: 'Read the manifest'
  },
  collection: {
    illustrator: 'Иллюстратор коллекции',
    about: 'Об иллюстраторе',
    links: 'Ссылки',
    videoLink: 'Видео создания коллекции',
    sale: 'Скидка',
    wallpaper: 'Обои',
    tshirt: 'Футболка',
    poster: 'Постер',
    instagram: {
      title: 'Эта коллекция в инстаграмме',
      footerText: 'Cфотографируйся в нашей продукции, размести фото с хештегом <span>#${tag}</span> и мы разместим твою фотографию в этом разделе.'
    }
  },
  announcement: {
    largeInputTitle: 'Оповестить об ежемесячном обновлении коллекций',
    shortInputTitle: 'Узнать первым',
    placeholder: 'Введите email',
    [TSHIRT]: {
      title: 'До выхода новой футболки осталось'
    },
    [POSTER]: {
      title: 'До выхода нового постера осталось'
    },
    [WALLPAPER]: {
      title: 'До выхода новых обоев осталось'
    }
  },
  common: {
    infoTable: 'Таблица размеров'
  }
}
