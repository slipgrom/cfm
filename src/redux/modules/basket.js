/* @flow */
// ------------------------------------
// Constants
// ------------------------------------
export const ADD_ITEM = 'ADD_ITEM'
export const REMOVE_ITEM = 'REMOVE_ITEM'
export const CHANGE_COUNT = 'CHANGE_COUNT'
export const CHANGE_DELIVERY_TYPE = 'CHANGE_DELIVERY_TYPE'
export const TOGGLE_REGISTER = 'TOGGLE_REGISTER'
export const CHANGE_SIZE = 'CHANGE_SIZE'
export const CHANGE_SEX = 'CHANGE_SEX'
export const CHANGE_STYLE = 'CHANGE_STYLE'

export const DELIVERY_TYPE_EMS = 'EMS'
export const DELIVERY_TYPE_COURIER = 'COURIER'

import {
  TSHIRT,
  POSTER
} from 'redux/modules/collections'

// export const SIZE_S = 'SIZE_S'
// export const SIZE_M = 'SIZE_M'
// export const SIZE_L = 'SIZE_L'
// export const SIZE_XL = 'SIZE_XL'
// export const SIZE_XXL = 'SIZE_XXL'
// ------------------------------------
// Actions
// ------------------------------------
// NOTE: "Action" is a Flow interface defined in https://github.com/TechnologyAdvice/flow-interfaces
// If you're unfamiliar with Flow, you are completely welcome to avoid annotating your code, but
// if you'd like to learn more you can check out: flowtype.org.
export const addItem = (item) => ({
  type: ADD_ITEM,
  payload: item
})
export const removeItem = (itemId) => ({
  type: REMOVE_ITEM,
  payload: itemId
})
export const changeCount = (index, count) => ({
  type: CHANGE_COUNT,
  payload: {
    index,
    count
  }
})
export const changeDeliveryType = (type) => ({
  type: CHANGE_DELIVERY_TYPE,
  payload: {
    currentDeliveryType: type
  }
})
export const changeSize = (index, size) => ({
  type: CHANGE_SIZE,
  payload: {
    index,
    size
  }
})
export const changeStyle = (index, style) => ({
  type: CHANGE_STYLE,
  payload: {
    index,
    style
  }
})
export const toggleRegister = () => ({ type: TOGGLE_REGISTER })

// ------------------------------------
// Action Handlers
// ------------------------------------
const ACTION_HANDLERS = {
  ADD_ITEM: (state, action) => {
    const newItems = [
      ...state.items,
      action.payload
    ]

    return Object.assign({}, state, {items: newItems})
  },
  REMOVE_ITEM: (state, action) => {
    const newState = [
      ...state.items.slice(0, action.payload),
      ...state.items.slice(action.payload + 1)
    ]

    return Object.assign({}, state, { items: newState })
  },
  CHANGE_COUNT: (state, action) => {
    const item = Object.assign({}, state.items[action.payload.index], {count: action.payload.count})

    const newState = [
      ...state.items.slice(0, action.payload.index),
      item,
      ...state.items.slice(action.payload.index + 1)
    ]

    return Object.assign({}, state, { items: newState })
  },
  TOGGLE_REGISTER: (state, action) => {
    return Object.assign({}, state, { register: !state.register })
  },
  CHANGE_DELIVERY_TYPE: (state, action) => {
    return Object.assign({}, state, action.payload)
  },
  CHANGE_SIZE: (state, action) => {
    const item = Object.assign({}, state.items[action.payload.index], {size: action.payload.size})

    const newState = [
      ...state.items.slice(0, action.payload.index),
      item,
      ...state.items.slice(action.payload.index + 1)
    ]

    return Object.assign({}, state, { items: newState })
  },
  CHANGE_STYLE: (state, action) => {
    const item = Object.assign({}, state.items[action.payload.index], {style: action.payload.style})

    const newState = [
      ...state.items.slice(0, action.payload.index),
      item,
      ...state.items.slice(action.payload.index + 1)
    ]

    return Object.assign({}, state, { items: newState })
  }

}

// ------------------------------------
// Reducer
// ------------------------------------
const initialState = {
  register: false,
  deliveryTypePrices: {
    [DELIVERY_TYPE_EMS]: 350,
    [DELIVERY_TYPE_COURIER]: 500
  },
  sizes: ['s', 'm', 'l', 'xl', 'xxl'],
  currentDeliveryType: DELIVERY_TYPE_EMS,
  items: [
    {
      size: 0,
      count: 1,
      style: 0,
      sex: 0,
      id: 1,
      collectionId: 1,
      collectionTitle: 'Супергерои уходят в отрыв',
      type: TSHIRT,
      price: 2500
    },
    {
      size: 1,
      count: 2,
      style: 0,
      sex: 1,
      id: 1,
      collectionId: 1,
      collectionTitle: 'Супергерои уходят в отрыв',
      type: POSTER,
      price: 2500
    }
  ]
}

export default function localesReducer (state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type]

  return handler ? handler(state, action) : state
}
