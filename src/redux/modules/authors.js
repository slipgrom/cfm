// ------------------------------------
// Constants
// ------------------------------------

// ------------------------------------
// Actions
// ------------------------------------

// ------------------------------------
// Action Handlers
// ------------------------------------
const ACTION_HANDLERS = {
}

// ------------------------------------
// Reducer
// ------------------------------------
const initialState = {
  0: {
    name: 'Покрас Лампас',
    imageSrc: '/images/authors/author1.png',
    about: 'Регулярная прецессия, в первом приближении, учитывает систематический уход. Если пренебречь малыми величинами, то видно, что механическая система искажает ускоряющийся гироскоп. Момент силы трения отличительно заставляет перейти к более сложной системе.',
    links: [
      'www.behance.net/pokras',
      'www.vk.com/pokraslampas'
    ]
  }
}

export default function reducer (state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type]

  return handler ? handler(state, action) : state
}
