// ------------------------------------
// Constants
// ------------------------------------

// ------------------------------------
// Actions
// ------------------------------------

// ------------------------------------
// Action Handlers
// ------------------------------------
const ACTION_HANDLERS = {
}

// ------------------------------------
// Reducer
// ------------------------------------
const initialState = {
  wallpapersList: [
    {
      collectionId: 1,
      modern: '/images/wallpaperSlider/mobila_01_01.png',
      vintage: '/images/wallpaperSlider/mobila_01_02.png'
    },
    {
      collectionId: 2,
      modern: '/images/wallpaperSlider/mobila_02_01.png',
      vintage: '/images/wallpaperSlider/mobila_02_02.png'
    },
    {
      collectionId: 3,
      modern: '/images/wallpaperSlider/mobila_03_01.png',
      vintage: '/images/wallpaperSlider/mobila_03_02.png'
    },
    {
      collectionId: 4,
      modern: '/images/wallpaperSlider/mobila_04_01.png',
      vintage: '/images/wallpaperSlider/mobila_04_02.png'
    },
    {
      collectionId: 5,
      modern: '/images/wallpaperSlider/mobila_05_01.png',
      vintage: '/images/wallpaperSlider/mobila_05_02.png'
    }
  ]
}

export default function reducer (state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type]

  return handler ? handler(state, action) : state
}
