/* @flow */
// ------------------------------------
// Constants
// ------------------------------------
export const TOGGLE_REGISTRATION_BLOCK = 'TOGGLE_REGISTRATION_BLOCK'
export const SET_REGISTRATION_FORM = 'SET_REGISTRATION_FORM'

// ------------------------------------
// Actions
// ------------------------------------
// NOTE: "Action" is a Flow interface defined in https://github.com/TechnologyAdvice/flow-interfaces
// If you're unfamiliar with Flow, you are completely welcome to avoid annotating your code, but
// if you'd like to learn more you can check out: flowtype.org.
export const toggleRegistrationBlock = (opened = true) => ({
  type: TOGGLE_REGISTRATION_BLOCK,
  payload: {}
})

export const setRegistrationForm = (formType) => ({
  type: SET_REGISTRATION_FORM,
  payload: {
    shownForm: formType
  }
})

// ------------------------------------
// Action Handlers
// ------------------------------------
const ACTION_HANDLERS = {
  TOGGLE_REGISTRATION_BLOCK: (state, action) => {
    const opened = !state.opened

    return Object.assign({}, state, {
      opened: opened
    })
  },
  SET_REGISTRATION_FORM: (state, action) => {
    return Object.assign({}, state, action.payload)
  }
}

export const REGISTRATION_FORM = 'registration'
export const LOGIN_FORM = 'login'
export const FORGET_PASS_FORM = 'forget_pass'

// ------------------------------------
// Reducer
// ------------------------------------
const initialState = {
  opened: false,
  shownForm: LOGIN_FORM
}

export default function menuReducer (state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type]

  return handler ? handler(state, action) : state
}
