import { combineReducers } from 'redux'
import { routeReducer as router } from 'react-router-redux'
import locales from './modules/locales'
import menus from './modules/menus'
import collections from './modules/collections'
import registration from './modules/registration'
import basket from './modules/basket'
import feedback from './modules/feedback'
import announcement from './modules/announcement'
import authors from './modules/authors'
import i18n from './modules/i18n'
import indexSlider from './modules/indexSlider'
import widgets from './modules/widgets'

export default combineReducers({
  menus,
  locales,
  registration,
  collections,
  announcement,
  basket,
  feedback,
  authors,
  i18n,
  indexSlider,
  widgets,
  router
})
