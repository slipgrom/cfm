import React, { PropTypes } from 'react'
import { Provider } from 'react-redux'
import { Router } from 'react-router'

export default class Root extends React.Component {
  static propTypes = {
    history: PropTypes.object.isRequired,
    routes: PropTypes.element.isRequired,
    store: PropTypes.object.isRequired
  };

  get content () {
    return (
      <Router history={this.props.history} onUpdate={() => window.scrollTo(0, 0)}>
        {this.props.routes}
      </Router>
    )
  }

  render () {
    return (
      <Provider store={this.props.store}>
        {this.content}
      </Provider>
    )
  }
}
